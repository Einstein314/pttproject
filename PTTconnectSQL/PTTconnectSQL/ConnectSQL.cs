﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Configuration;

namespace PTTconnectSQL
{
    public class ConnectSQL : IDisposable
    {
        private SqlConnection sqlConnection;
        string constr = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString; //加參考、App.config
        
        public ConnectSQL()
        {
            sqlConnection = new SqlConnection(constr);
            sqlConnection.Open();
        }
      

        public SqlCommand createSqlCommand(String sql)
        {
            SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);
            return sqlCommand;
        }

        public void Dispose()
        {
            this.sqlConnection.Close(); 
        }
    }
}
