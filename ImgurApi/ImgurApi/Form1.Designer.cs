﻿namespace ImgurApi
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.取得照片 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // 取得照片
            // 
            this.取得照片.Location = new System.Drawing.Point(89, 86);
            this.取得照片.Name = "取得照片";
            this.取得照片.Size = new System.Drawing.Size(155, 71);
            this.取得照片.TabIndex = 0;
            this.取得照片.Text = "取得照片";
            this.取得照片.UseVisualStyleBackColor = true;
            this.取得照片.Click += new System.EventHandler(this.取得照片_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(669, 366);
            this.Controls.Add(this.取得照片);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button 取得照片;
    }
}

