﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImgurApi
{
    public class Student
    {
        public string Name { get; set; }
        public int Number { get; set; }
    }
}
