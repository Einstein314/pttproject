﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImgurApi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void 取得照片_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string filePath="";
            openFileDialog.InitialDirectory = @"C:\Users\Einstein\Desktop";
            openFileDialog.Filter = "圖片檔(*.jpg)|*.jpg|圖片檔(*.png)|*.png";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {                   
                 filePath = openFileDialog.FileName;
                 Console.WriteLine(filePath);                   
            }
            
            //建立 WebRequest 並指定目標的 uri
            WebRequest request = WebRequest.Create("https://api.imgur.com/3/upload");
            request.Headers.Add(HttpRequestHeader.Authorization, "Bearer a5da701663e4a9a67318681f03ed04214c1ba90e");
            request.Method = "POST";
            object postData = new { image = File.ReadAllBytes(filePath) };
            //將需 post 的資料內容轉為 stream 
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))   //類似Python的with
            {
                string json = JsonConvert.SerializeObject(postData);
                streamWriter.Write(json);
                streamWriter.Flush();
            }
            request.ContentType = "application/json; charset=utf-8";      
            using (var httpResponse = (HttpWebResponse)request.GetResponse())
            //使用 GetResponseStream 方法從 server 回應中取得資料，stream 必需被關閉
            //使用 stream.close 就可以直接關閉 WebResponse 及 stream，但同時使用 using 或是關閉兩者並不會造成錯誤，養成習慣遇到其他情境時就比較不會出錯
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine(result);
            }


            //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //{
            //    var result = streamReader.ReadToEnd();

            //    Console.WriteLine(JsonConvert.DeserializeObject<Imgur>(result).data[0].id);

            //}

            //var data = new
            //{
            //    a=100,
            //    s="hey"
            //};

            //Student student = new Student()
            //{
            //    Name = "Mike",
            //    Number = 28
            //};


            //Console.WriteLine(data);
            //Console.WriteLine(student);
            //Console.WriteLine(JsonConvert.SerializeObject(student)); //序列化
            //string strObj = JsonConvert.SerializeObject(student);
            //Console.WriteLine(JsonConvert.DeserializeObject<Student>(strObj)); //反序列化
            //Console.WriteLine(JsonConvert.DeserializeObject(strObj));
            //Console.WriteLine(JsonConvert.DeserializeObject<Student>(strObj).Name);
        }
    }
}
