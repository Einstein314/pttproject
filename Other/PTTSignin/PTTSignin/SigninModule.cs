﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;

namespace PTTSignin
{
    public class SigninModule : ConnectSQL
    {
        public SqlDataReader Signin(string email, string password)
        {
            string sql = $"select U_Id from PTTUser where email=@email and password=@password";
            SqlCommand command = this.createSqlCommand(sql);
            command.Parameters.Add("@email", email);
            command.Parameters.Add("@password", password);

            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }

        public void SigninAddScore(int id)
        {
            
        }
    }
}
