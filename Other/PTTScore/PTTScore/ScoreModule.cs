﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;

namespace PTTScore
{
    public class ScoreModule : ConnectSQL //一旦繼承 先執行副類建構元 SqlConnect已開啟 外部一但有人new 就會開啟串流
    {
        public SqlDataReader GetCurrentScore(int id)
        {
            string currentscore = $"select score from PTTUser where U_Id = {id}";
            SqlCommand command = this.createSqlCommand(currentscore);
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }

        public SqlDataReader GetSigninDate(int id)
        {
            string currentday = $"select signindate from PTTUser where U_Id={id}";
            SqlCommand command = this.createSqlCommand(currentday);
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }

        public void AddScoreBySignin(int id)
        {
            int score;
            using (ScoreModule module = new ScoreModule())
            {
                SqlDataReader reader = module.GetCurrentScore(id);
                reader.Read();
                score = Int32.Parse(reader["score"].ToString());
            }

            string sql = $@"update PTTUser 
                            set score={score + 1}
                            where U_Id={id}";
            SqlCommand command = this.createSqlCommand(sql);
            command.ExecuteNonQuery();        
        }

        public void AddSigninDay(int id)
        {
            int day;
            using (ScoreModule module = new ScoreModule())
            {
                SqlDataReader reader = module.GetSigninDate(id);
                reader.Read();
                day = Int32.Parse(reader["signindate"].ToString());
            }

            string sql = $@"update PTTUser set signindate={day + 1} where U_Id={id}";
            SqlCommand command = this.createSqlCommand(sql);                                        
            command.ExecuteNonQuery();                                       
        }
     
        public SqlDataReader CheckISIT(int id)
        {
            string sql = $"select issignintoday from PTTUser where U_Id={id}";
            SqlCommand command = this.createSqlCommand(sql);
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }

        public void SetISIT(int id)
        {
            string sql = $"update PTTUser set issignintoday=1 where U_Id={id}";
            SqlCommand command = this.createSqlCommand(sql);
            command.ExecuteNonQuery();
        }
    }
}
