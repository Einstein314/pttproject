﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTTScore 
{
    public class SigninDispose : ScoreModule 
    {
        public void FirstSignin(int id)
        {
            this.AddScoreBySignin(id);
            this.AddSigninDay(id);
            this.SetISIT(id); //須關閉串流       
        }
    }
}
