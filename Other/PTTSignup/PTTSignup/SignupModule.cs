﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;
using System.Text.RegularExpressions;

namespace PTTSignup
{
    public class SignupModule : ConnectSQL //有實作IDispose 可用using
    {
        public int Signup(string name, string email, string password, string date)
        {
            string sql = $@"insert into PTTUser(email,password,name,date,U_pic,score,issignintoday,signindate)
                            values('{email}','{password}','{name}','{date}','C:\Users\Einstein\Source\Repos\PTTproject\Resources\user.png',0,0,0)"; //mssql有增加識別項 因此不用insert id
            SqlCommand command = this.createSqlCommand(sql);
            int reader = command.ExecuteNonQuery();

            return reader;
        }

        public bool IsValidEmail(string email)
        {
            try
            {
                //var add = new System.Net.Mail.MailAddress(email);
                //return add.Address == email;
                string emailRule = @"^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$"; //詳情看OneNote email格式
                return Regex.IsMatch(email,emailRule);
            }
            catch 
            {
                return false;
            }
        }
    }
}
