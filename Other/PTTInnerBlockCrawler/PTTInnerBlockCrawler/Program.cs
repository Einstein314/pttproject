﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using PTTconnectSQL;

namespace PTTInnerBlockCrawler
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver();          
            using (ConnectSQL connect = new ConnectSQL())
            {
                SqlCommand command1, command2; 
                for(int i=1; i<=128; i++)
                {
                    //從HB取得名稱並前往URL
                    string URLname = $@" select Name_E from HotBlock where HB_Id={i}";
                    command1 = connect.createSqlCommand(URLname);                   
                    SqlDataReader reader = command1.ExecuteReader();
                    reader.Read();
                    string Name_E = reader["Name_E"].ToString();
                    Console.WriteLine(Name_E);
                    reader.Close();

                    string URL = $@"https://www.ptt.cc/bbs/{Name_E}/index.html";
                    Console.WriteLine(URL);

                    driver.Navigate().GoToUrl(URL);
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);
                   
                    //判斷是否有18+限制
                    if (driver.Url.ToString().Equals($"https://www.ptt.cc/ask/over18?from=%2Fbbs%2F{Name_E}%2Findex.html"))
                    {
                        var yes = driver.FindElement(By.CssSelector(" div.over18-button-container:nth-child(2) > button:nth-child(1) "));
                        Console.WriteLine("18+");
                        yes.Click();
                    }              
                    
                    //撈資料 此為第一筆
                    Thread.Sleep(500);  
                    Re:
                    //for(int j=1; j<=53; j++) //固定筆數1113
                    //{
                        var innerblock = driver.FindElements(By.ClassName("r-ent")); //一頁21筆
                        foreach (var board in innerblock)
                        {
                            string title = board.FindElement(By.ClassName("title")).Text;
                            string author = board.FindElement(By.ClassName("author")).Text;
                            string date = board.FindElement(By.ClassName("date")).Text;
                            Console.WriteLine(title + " " + author + " " + date + " " + i);

                            string sql = $@"insert into InnerBlock(Title,Author,date,HB_Id)
                                        values('{title}', '{author}', '{date}', {i})";
                            try
                            {
                                command2 = connect.createSqlCommand(sql);
                                command2.ExecuteNonQuery();
                            }
                            catch
                            {

                            }
                        }
                        var previous = driver.FindElement(By.CssSelector("#action-bar-container > div > div.btn-group.btn-group-paging > a:nth-child(2)"));
                        if (previous.Enabled) //判斷上頁是否可按
                        {
                            previous.Click();
                            goto Re; //全部筆數
                        }
                    //}
                    
                }                             
            }
            Console.ReadKey();
        }
    }
}
