﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;
using System.Data.SqlClient;

namespace PTTHotBlockCrawler
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.ptt.cc/bbs/hotboards.html");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000); //等待直到出現 顯示出現毫秒數的精準度

            using (ConnectSQL connect = new ConnectSQL())
            {               
                SqlCommand command;
                int change;
                for (int i = 1; i <= 128; i++)
                {
                    var Name_E = driver.FindElement(By.CssSelector($@" div:nth-child({i}) > a > div.board-name ")).Text;
                    var number = driver.FindElement(By.CssSelector($@" div:nth-child({i}) > a > div.board-nuser > span ")).Text;
                    var Name_C = driver.FindElement(By.CssSelector($@" div:nth-child({i}) > a > div.board-class ")).Text;
                    var discription = driver.FindElement(By.CssSelector($@" div:nth-child({i}) > a > div.board-title ")).Text;

                    string sql = $@"insert into HotBlock(Name_E, number, Name_C, discription)
                                    values('{Name_E}', '{number}', '{Name_C}', '{discription}')";
                    command = connect.createSqlCommand(sql);
                    change = command.ExecuteNonQuery();
                    Console.WriteLine(change);                  
                }                             
            } Console.ReadKey();
                                    
        }
    }
}
