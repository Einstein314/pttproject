﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTTArticleCrawler
{
    public class ElementFunction 
    {      
        public static bool IsElementExist(IWebDriver driver, By by)
        {
            try
            {              
                driver.FindElement(by);               
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
            
        }
        public static bool IsElementExist(IWebElement element, By by)
        {
            try
            {
                element.FindElement(by);
                return true;
            }
            catch(Exception e)
            {

                return false;
            }
        }

        
    }
}
