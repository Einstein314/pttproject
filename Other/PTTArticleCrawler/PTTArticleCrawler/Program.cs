﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Data.SqlClient;
using System.Threading;
using OpenQA.Selenium.Interactions; //可按enter鍵
using System.Net;

namespace PTTArticleCrawler
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver();
            Actions builder = new Actions(driver);

            using (ConnectSQL connect = new ConnectSQL())
            {
                int record_aid = 17463; //每次需調整 一次性使用 紀錄：第17463筆文章(上次的最後一筆)(下一次抓的文章是第9036筆)
                for (int i=18; i<=128; i++) //每次需調整 或是按任意鍵繼續 紀錄：第十八區塊 下次直接按執行即可
                {
                    SqlCommand command_getnamee, command_gettitlesinfo, command_getnum, command_insertarticle, command_insertpic, command_insertcomment, command_ignore;
                    //可用子搜尋化簡
                    
                    //從HB取得名稱並前往URL
                    string URLname = $@" select Name_E from HotBlock where HB_Id={i}";
                    command_getnamee = connect.createSqlCommand(URLname);
                    SqlDataReader reader1 = command_getnamee.ExecuteReader();
                    reader1.Read();
                    string Name_E = reader1["Name_E"].ToString();
                    Console.WriteLine(Name_E);
                    reader1.Close();

                    string URL = $@"https://www.ptt.cc/bbs/{Name_E}/index.html";
                    Console.WriteLine(URL);

                    driver.Navigate().GoToUrl(URL);
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10000);

                    //判斷是否有18+限制
                    if (driver.Url.ToString().Equals($"https://www.ptt.cc/ask/over18?from=%2Fbbs%2F{Name_E}%2Findex.html"))
                    {
                        var yes = driver.FindElement(By.CssSelector(" div.over18-button-container:nth-child(2) > button:nth-child(1) "));
                        Console.WriteLine("18+");
                        yes.Click();
                    }
                    Thread.Sleep(500);

                    //搜尋文章---------------------------------------------------------------------------
                    string gettitlesinfo = $"select IB_Id,Title,Author from InnerBlock where HB_Id={i}";//待確認
                    string getnum = $"select COUNT(*) Title from InnerBlock where HB_Id = {i}";                 

                    //搜尋總筆數
                    command_getnum = connect.createSqlCommand(getnum);
                    int count = (int)command_getnum.ExecuteScalar();
                   
                    //搜尋標題
                    command_gettitlesinfo = connect.createSqlCommand(gettitlesinfo);
                    SqlDataReader reader2 = command_gettitlesinfo.ExecuteReader();                                      
                    List<Titles> titles = new List<Titles>();
                    while(reader2.Read())
                    {
                        Titles ts = new Titles();

                        ts.IB_Id = Int32.Parse(reader2["IB_Id"].ToString());
                        ts.eachtitle = reader2["Title"].ToString();
                        ts.author = reader2["Author"].ToString();

                        titles.Add(ts);
                    }                  
                    reader2.Close();

                    //輸入標題並查詢
                    Console.WriteLine(count+"------------------------------------"); //總筆數
                    foreach (Titles t in titles)
                    {
                        //輸入字串                                   
                        var searchbar = driver.FindElement(By.CssSelector("#search-bar > input"));
                        searchbar.Clear();
                        searchbar.SendKeys(t.eachtitle);
                        searchbar.SendKeys(Keys.Enter);
                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5); //等待5秒 若沒有文章 5秒後 繼續執行   

                        //進入頁面     
                        if (!ElementFunction.IsElementExist(driver, By.CssSelector(" div.r-ent > div.title > a")))//如果沒有文章
                        {
                            string sql_forignorethisarticle = $@"insert into Article(date,author,title,words,IB_Id,HB_Id) 
                                                                values('不存在','不存在','不存在','不存在','{t.IB_Id}','{i}')";
                            command_ignore = connect.createSqlCommand(sql_forignorethisarticle);
                            
                            command_ignore.ExecuteNonQuery();
                            record_aid++;
                            continue;        
                        }
                        var test = driver.FindElement(By.CssSelector(" div.r-ent > div.title > a"));
                        driver.Navigate().GoToUrl(test.GetAttribute("href"));  //出錯 HB_Id: 30 A_Id: 30148                  
                        Thread.Sleep(600);

                        //取得精確時間及文章
                        string date;                   
                        if (!ElementFunction.IsElementExist(driver, By.CssSelector("div:nth-child(4) > span.article-meta-value"))) //有沒時間的例外
                        {
                            date = "不存在";
                        }else
                        {
                            date = driver.FindElement(By.CssSelector("div:nth-child(4) > span.article-meta-value")).Text;
                        }

                        string maintext = "";
                        if(!ElementFunction.IsElementExist(driver, By.CssSelector("#main-content")))
                        {
                            maintext = "404 Not Found";
                        }else
                        {
                            var allwordselement = driver.FindElement(By.CssSelector("#main-content")).Text; //先撈全部
                            maintext = allwordselement.Replace("※ 發信站", "※※※").Split(new char[3] { '※', '※', '※' })[0]; //切除留言以下
                        }                                   
                             
                        //寫入資料庫(先寫入文章在寫入照片--因為有A_Id)                        
                        string sql_forarticle = $@"insert into Article(date,author,title,words,IB_Id,HB_Id) 
                                                   values('{date}','{t.author}','{t.eachtitle}','{maintext}','{t.IB_Id}','{i}')"; //還有U_Id
                        command_insertarticle = connect.createSqlCommand(sql_forarticle);
                        //避免sql不合法的最懶處理
                        try
                        {
                            command_insertarticle.ExecuteNonQuery();
                        }catch
                        {
                            maintext = "有sql injection問題 因此無法提供文章資訊";
                            string sql_forinjectionproblem = $@"insert into Article(date,author,title,words,IB_Id,HB_Id) 
                                                                values('{maintext}','{maintext}','{maintext}','{maintext}','{t.IB_Id}','{i}')";
                            command_insertarticle = connect.createSqlCommand(sql_forinjectionproblem);
                            command_insertarticle.ExecuteNonQuery();
                        }
                        record_aid++;
                        Console.WriteLine(date + " " + t.author + " " + t.eachtitle + "\r\n" + maintext + " IB_Id: " + t.IB_Id + " HB_Id: " + i + " A_Id: " + record_aid);
                       
                        //抓照片
                        var a = driver.FindElements(By.TagName("a")); //先撈所有連結字串
                        if(a.Count>0)
                        {
                            foreach (var eacha in a)
                            {
                                var link = eacha.Text;
                                var piclink = link.Substring(link.Length - 3, 3);
                                if (piclink.Equals("jpg") || piclink.Equals("png") || piclink.Equals("bmp")) //判斷是否為照片
                                {
                                    if (maintext.Contains(link)) // 判斷文章中是否包含所取的相片
                                    {
                                        Console.WriteLine("This is what I want --> " + link + $" A_Id={record_aid}");
                                        string sql_forApic = $@"insert into A_pic(A_Id, pic_link) values({record_aid}, '{link}')";
                                        command_insertpic = connect.createSqlCommand(sql_forApic);
                                        command_insertpic.ExecuteNonQuery();
                                    }
                                }
                            }
                        }                      

                        //取得留言
                        var allpush = driver.FindElements(By.ClassName("push"));
                        if(allpush.Count>0)
                        {
                            foreach (var eachpush in allpush)
                            {
                                string type, username, IpDateTime;
                                StringBuilder no_pic_comment;
                                try
                                {
                                    type = eachpush.FindElement(By.XPath("span[1]")).Text;
                                    username = eachpush.FindElement(By.XPath("span[2]")).Text;

                                    Console.Write("\r\n" + type + " " + username);

                                    //沒有照片
                                    no_pic_comment = new StringBuilder(eachpush.FindElement(By.XPath("span[3]")).Text);

                                    //如果有連結 不好用 瑕疵程式
                                    //if(ElementFunction.IsElementExist(eachpush, By.TagName("a"))) //////////
                                    //{
                                    //    var a_comment = eachpush.FindElement(By.TagName("a")).Text;
                                    //    if (a_comment.Contains("jpg") || a_comment.Contains("png") || a_comment.Contains("bmp")) //判斷是否為照片
                                    //    {
                                    //        Console.WriteLine("This is a piccomment --> "+a_comment);
                                    //    }
                                    //    else
                                    //    {
                                    //        no_pic_comment.Append(a_comment); 
                                    //        Console.WriteLine(no_pic_comment);
                                    //    }
                                    //}
                                    Console.Write(no_pic_comment);

                                    IpDateTime = eachpush.FindElement(By.XPath("span[4]")).Text; ///////////
                                    Console.WriteLine(IpDateTime);
                                }
                                catch
                                {
                                    type = "檔案過大 無法顯示！";
                                    username = "檔案過大 無法顯示！";
                                    IpDateTime = "檔案過大 無法顯示！";
                                    no_pic_comment = new StringBuilder("檔案過大 無法顯示！"); ;
                                }
                                string sql_forcomment = $@"insert into Comment(A_Id,username,type,comment,IpDateTime)
                                                       values('{record_aid}','{username}','{type}','{no_pic_comment}','{IpDateTime}')";
                                command_insertcomment = connect.createSqlCommand(sql_forcomment);
                                try
                                {
                                    command_insertcomment.ExecuteNonQuery();
                                }
                                catch
                                {
                                    string newcomment = "由於sql語法問題 無法顯示";
                                    string newusername = "由於sql語法問題 無法顯示";
                                    string newtype = "由於sql語法問題 無法顯示";
                                    string newIpDateTime = "由於sql語法問題 無法顯示";
                                    string sql_forcommentproblem = $@"insert into Comment(A_Id,username,type,comment,IpDateTime)
                                                                  values('{record_aid}','{newusername}','{newtype}','{newcomment}','{newIpDateTime}')";
                                    command_insertcomment = connect.createSqlCommand(sql_forcommentproblem);                                    
                                    command_insertcomment.ExecuteNonQuery();                                                                
                                }
                            }
                        }

                        //回上一頁
                        Thread.Sleep(300);
                        driver.Navigate().Back();
                        Thread.Sleep(300);
                        //Console.ReadKey(); //按一下再執行一個文章
                    }
                    //Console.ReadKey(); 按一下再執行一個區塊
                    //搜尋文章---------------------------------------------------------------------------
                }
                
            }

           
        }      
    }
}
