﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;

namespace PTTBlockModule
{
    public class InnerBlockModule : ConnectSQL
    {
        public int GetInnerBlockQuantity(int hbid)
        {
            using(ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select COUNT(*) from InnerBlock where HB_Id={hbid}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();

                return Int32.Parse(reader[""].ToString());
            }
        }          
        
        public int GetFirstIB_Id(int hbid)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select TOP 1 IB_Id from InnerBlock where HB_Id={hbid}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();

                return Int32.Parse(reader["IB_Id"].ToString());
            }
        }

        public string GetTitle(int IB_Id)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select Title from InnerBlock where IB_Id = {IB_Id}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();

                return reader["Title"].ToString();
            }
        }

        public string GetAuthor(int IB_Id)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select Author from InnerBlock where IB_Id = {IB_Id}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();

                return reader["Author"].ToString();            
            }
        }

        public string GetDate(int IB_Id)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select Date from InnerBlock where IB_Id = {IB_Id}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();

                return reader["Date"].ToString();
            }
        }
    }
}
