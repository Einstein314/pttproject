﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;

namespace PTTBlockModule 
{
    public class CommentModule: ConnectSQL
    {
        public List<Comment> GetComment(int aid)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                List<Comment> list = new List<Comment>();
                string sql = $"select * from Comment where A_Id = {aid}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                while(reader.Read())
                {
                    Comment cm = new Comment();
                    cm.C_Id = Int32.Parse(reader["C_Id"].ToString());
                    cm.A_Id = aid;
                    cm.username = reader["username"].ToString();
                    cm.type = reader["type"].ToString();
                    cm.comment = reader["comment"].ToString();
                    cm.IpDateTime = reader["IpDateTime"].ToString();

                    list.Add(cm);
                }                          
                return list;
            }              
        }
    }
}
