﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;

namespace PTTBlockModule
{
    public class HotBlockModule : ConnectSQL
    {
        public string GetName_E(int hbid)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select Name_E from HotBlock where HB_Id = {hbid}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                return reader["Name_E"].ToString();
            }                   
        }

        public int GetNumber(int hbid)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select number from HotBlock where HB_Id = {hbid}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                return Int32.Parse(reader["number"].ToString());
            }                        
        }

        public string GetName_C(int hbid)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select Name_C from HotBlock where HB_Id = {hbid}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                return reader["Name_C"].ToString();
            }              
        }

        public string GetDescription(int hbid)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select discription from HotBlock where HB_Id = {hbid}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();
                return reader["discription"].ToString();
            }                           
        }
    }
}
