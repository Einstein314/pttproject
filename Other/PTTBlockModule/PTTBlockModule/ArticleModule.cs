﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;

namespace PTTBlockModule
{
    public class ArticleModule : ConnectSQL
    {
        public string GetDate(int ibid)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select date from Article where IB_Id = {ibid}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();

                return reader["date"].ToString();
            }           
        }

        public string GetTitle(int ibid)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select title from Article where IB_Id = {ibid}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();

                return reader["title"].ToString();
            }
        }

        public string GetAuthor(int ibid)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select author from Article where IB_Id = {ibid}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();

                return reader["author"].ToString();
            }
        }

        public string GetHBType(int ibid)
        {
            using (HotBlockModule module = new HotBlockModule())
            {
                string sql = $"select HB_Id from Article where IB_Id = {ibid}";
                SqlCommand command = module.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();

                return module.GetName_E(Int32.Parse(reader["HB_Id"].ToString()));
            }
        }

        public string GetWords(int ibid)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select words from Article where IB_Id = {ibid}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();

                return reader["words"].ToString();
            }
        }

        public int GetA_Id(int ibid)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select A_Id from Article where IB_Id = {ibid}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();

                return Int32.Parse(reader["A_Id"].ToString());
            }
        }
    }
}
