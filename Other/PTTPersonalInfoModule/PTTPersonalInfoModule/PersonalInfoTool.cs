﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;
using PTTSignup;

namespace PTTPersonalInfoModule
{
    public class PersonalInfoTool : ConnectSQL
    {
        public SqlDataReader GetPersonalInfo(int id)
        {
            string sql = $"select * from PTTUser where U_id={id}";
            SqlCommand command = this.createSqlCommand(sql);
            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }

        public int SetPersonalInfo(int id, string pic, string name, string email, string password, string born)
        {
           
            SignupModule signup = new SignupModule();                
            if (signup.IsValidEmail(email))
            {
               string sql = $@"update PTTUser 
                               set U_pic='{pic}', name='{name}', email='{email}', password='{password}', date='{born}'
                               where U_id={id}";
               SqlCommand command = this.createSqlCommand(sql);
               int reader = command.ExecuteNonQuery();  
               return reader;
                  
            }else
            {
               return 0;
            }                            
        }    
    }
}
