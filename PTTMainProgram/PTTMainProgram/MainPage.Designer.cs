﻿namespace PTTMainProgram
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.personalinfo = new System.Windows.Forms.Button();
            this.NavPanel = new System.Windows.Forms.Panel();
            this.BreadCrumb = new System.Windows.Forms.Panel();
            this.HBcrumb = new System.Windows.Forms.Label();
            this.Home = new System.Windows.Forms.Label();
            this.next_previous_panel = new System.Windows.Forms.Panel();
            this.Previous = new System.Windows.Forms.Button();
            this.Next = new System.Windows.Forms.Button();
            this.Frontest = new System.Windows.Forms.Button();
            this.Behindest = new System.Windows.Forms.Button();
            this.HomePanel = new System.Windows.Forms.Panel();
            this.TestInnerPanel = new System.Windows.Forms.Panel();
            this.TestBox = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TestArticlePanel = new System.Windows.Forms.Panel();
            this.main_artecle = new System.Windows.Forms.Panel();
            this.TestArticleBar = new System.Windows.Forms.Panel();
            this.TestHBContainer = new System.Windows.Forms.Panel();
            this.TestHBContainer2 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TestArticleInfo = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.標題 = new System.Windows.Forms.Label();
            this.Author = new System.Windows.Forms.Label();
            this.metroStyleExtender1 = new MetroFramework.Components.MetroStyleExtender(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.WriteArticle = new System.Windows.Forms.ToolStripMenuItem();
            this.writecomment = new System.Windows.Forms.ToolStripMenuItem();
            this.NavPanel.SuspendLayout();
            this.BreadCrumb.SuspendLayout();
            this.next_previous_panel.SuspendLayout();
            this.TestInnerPanel.SuspendLayout();
            this.TestBox.SuspendLayout();
            this.TestArticlePanel.SuspendLayout();
            this.TestArticleBar.SuspendLayout();
            this.TestHBContainer.SuspendLayout();
            this.TestHBContainer2.SuspendLayout();
            this.TestArticleInfo.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // personalinfo
            // 
            this.personalinfo.Dock = System.Windows.Forms.DockStyle.Right;
            this.personalinfo.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.personalinfo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.personalinfo.Location = new System.Drawing.Point(1074, 0);
            this.personalinfo.Name = "personalinfo";
            this.personalinfo.Size = new System.Drawing.Size(83, 45);
            this.personalinfo.TabIndex = 1;
            this.personalinfo.Text = "個人資料";
            this.personalinfo.UseVisualStyleBackColor = true;
            this.personalinfo.Click += new System.EventHandler(this.personalinfo_Click);
            // 
            // NavPanel
            // 
            this.NavPanel.AccessibleRole = System.Windows.Forms.AccessibleRole.Client;
            this.NavPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.NavPanel.Controls.Add(this.BreadCrumb);
            this.NavPanel.Controls.Add(this.next_previous_panel);
            this.NavPanel.Controls.Add(this.personalinfo);
            this.NavPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.NavPanel.Location = new System.Drawing.Point(23, 50);
            this.NavPanel.Name = "NavPanel";
            this.NavPanel.Size = new System.Drawing.Size(1157, 45);
            this.NavPanel.TabIndex = 7;
            // 
            // BreadCrumb
            // 
            this.BreadCrumb.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.BreadCrumb.Controls.Add(this.HBcrumb);
            this.BreadCrumb.Controls.Add(this.Home);
            this.BreadCrumb.Dock = System.Windows.Forms.DockStyle.Left;
            this.BreadCrumb.Location = new System.Drawing.Point(0, 0);
            this.BreadCrumb.Name = "BreadCrumb";
            this.BreadCrumb.Size = new System.Drawing.Size(409, 45);
            this.BreadCrumb.TabIndex = 8;
            // 
            // HBcrumb
            // 
            this.HBcrumb.Cursor = System.Windows.Forms.Cursors.Hand;
            this.HBcrumb.Dock = System.Windows.Forms.DockStyle.Left;
            this.HBcrumb.Enabled = false;
            this.HBcrumb.Font = new System.Drawing.Font("微軟正黑體", 15.75F);
            this.HBcrumb.ForeColor = System.Drawing.Color.White;
            this.HBcrumb.Location = new System.Drawing.Point(156, 0);
            this.HBcrumb.Name = "HBcrumb";
            this.HBcrumb.Size = new System.Drawing.Size(221, 45);
            this.HBcrumb.TabIndex = 2;
            this.HBcrumb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.HBcrumb.Click += new System.EventHandler(this.HBcrumb_Click);
            // 
            // Home
            // 
            this.Home.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Home.Dock = System.Windows.Forms.DockStyle.Left;
            this.Home.Font = new System.Drawing.Font("微軟正黑體", 15.75F);
            this.Home.ForeColor = System.Drawing.Color.Yellow;
            this.Home.Location = new System.Drawing.Point(0, 0);
            this.Home.Name = "Home";
            this.Home.Size = new System.Drawing.Size(156, 45);
            this.Home.TabIndex = 1;
            this.Home.Text = "批踢踢實業\'仿\'";
            this.Home.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Home.Click += new System.EventHandler(this.Home_Click);
            // 
            // next_previous_panel
            // 
            this.next_previous_panel.Controls.Add(this.Previous);
            this.next_previous_panel.Controls.Add(this.Next);
            this.next_previous_panel.Controls.Add(this.Frontest);
            this.next_previous_panel.Controls.Add(this.Behindest);
            this.next_previous_panel.Dock = System.Windows.Forms.DockStyle.Right;
            this.next_previous_panel.Location = new System.Drawing.Point(774, 0);
            this.next_previous_panel.Name = "next_previous_panel";
            this.next_previous_panel.Size = new System.Drawing.Size(300, 45);
            this.next_previous_panel.TabIndex = 8;
            this.next_previous_panel.Visible = false;
            // 
            // Previous
            // 
            this.Previous.Dock = System.Windows.Forms.DockStyle.Left;
            this.Previous.Location = new System.Drawing.Point(75, 0);
            this.Previous.Name = "Previous";
            this.Previous.Size = new System.Drawing.Size(75, 45);
            this.Previous.TabIndex = 5;
            this.Previous.Text = "上一頁";
            this.Previous.UseVisualStyleBackColor = true;
            this.Previous.Click += new System.EventHandler(this.Previous_Click);
            // 
            // Next
            // 
            this.Next.Dock = System.Windows.Forms.DockStyle.Right;
            this.Next.Location = new System.Drawing.Point(150, 0);
            this.Next.Name = "Next";
            this.Next.Size = new System.Drawing.Size(75, 45);
            this.Next.TabIndex = 6;
            this.Next.Text = "下一頁";
            this.Next.UseVisualStyleBackColor = true;
            this.Next.Click += new System.EventHandler(this.Next_Click);
            // 
            // Frontest
            // 
            this.Frontest.Dock = System.Windows.Forms.DockStyle.Left;
            this.Frontest.Location = new System.Drawing.Point(0, 0);
            this.Frontest.Name = "Frontest";
            this.Frontest.Size = new System.Drawing.Size(75, 45);
            this.Frontest.TabIndex = 8;
            this.Frontest.Text = "最前頁";
            this.Frontest.UseVisualStyleBackColor = true;
            this.Frontest.Click += new System.EventHandler(this.Frontest_Click);
            // 
            // Behindest
            // 
            this.Behindest.Dock = System.Windows.Forms.DockStyle.Right;
            this.Behindest.Location = new System.Drawing.Point(225, 0);
            this.Behindest.Name = "Behindest";
            this.Behindest.Size = new System.Drawing.Size(75, 45);
            this.Behindest.TabIndex = 7;
            this.Behindest.Text = "最後頁";
            this.Behindest.UseVisualStyleBackColor = true;
            this.Behindest.Click += new System.EventHandler(this.behindest_Click);
            // 
            // HomePanel
            // 
            this.HomePanel.AutoScroll = true;
            this.HomePanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.HomePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HomePanel.Location = new System.Drawing.Point(23, 95);
            this.HomePanel.Name = "HomePanel";
            this.HomePanel.Size = new System.Drawing.Size(1157, 477);
            this.HomePanel.TabIndex = 4;
            this.HomePanel.VisibleChanged += new System.EventHandler(this.BackHomePanel);
            // 
            // TestInnerPanel
            // 
            this.TestInnerPanel.AutoScroll = true;
            this.TestInnerPanel.BackColor = System.Drawing.Color.Transparent;
            this.TestInnerPanel.Controls.Add(this.TestBox);
            this.TestInnerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TestInnerPanel.Location = new System.Drawing.Point(23, 95);
            this.TestInnerPanel.Name = "TestInnerPanel";
            this.TestInnerPanel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TestInnerPanel.Size = new System.Drawing.Size(1157, 477);
            this.TestInnerPanel.TabIndex = 5;
            this.TestInnerPanel.Visible = false;
            // 
            // TestBox
            // 
            this.TestBox.Controls.Add(this.label4);
            this.TestBox.Controls.Add(this.label3);
            this.TestBox.Controls.Add(this.label2);
            this.TestBox.Location = new System.Drawing.Point(0, 34);
            this.TestBox.Name = "TestBox";
            this.TestBox.Size = new System.Drawing.Size(1042, 85);
            this.TestBox.TabIndex = 6;
            this.TestBox.TabStop = false;
            this.TestBox.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(950, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 24);
            this.label4.TabIndex = 9;
            this.label4.Text = "我是日期";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(7, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "我是作者";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(6, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(877, 30);
            this.label2.TabIndex = 7;
            this.label2.Text = "我是標題我是標題我是標題我是標題我是標題我是標題我是標題我是標題我是標題";
            this.label2.UseMnemonic = false;
            // 
            // TestArticlePanel
            // 
            this.TestArticlePanel.AutoScroll = true;
            this.TestArticlePanel.Controls.Add(this.main_artecle);
            this.TestArticlePanel.Controls.Add(this.TestArticleBar);
            this.TestArticlePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TestArticlePanel.Location = new System.Drawing.Point(23, 95);
            this.TestArticlePanel.Name = "TestArticlePanel";
            this.TestArticlePanel.Size = new System.Drawing.Size(1157, 477);
            this.TestArticlePanel.TabIndex = 7;
            this.TestArticlePanel.Visible = false;
            // 
            // main_artecle
            // 
            this.main_artecle.AutoScroll = true;
            this.main_artecle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_artecle.Location = new System.Drawing.Point(0, 100);
            this.main_artecle.Name = "main_artecle";
            this.main_artecle.Size = new System.Drawing.Size(1157, 377);
            this.main_artecle.TabIndex = 1;
            // 
            // TestArticleBar
            // 
            this.TestArticleBar.BackColor = System.Drawing.Color.Navy;
            this.TestArticleBar.Controls.Add(this.TestHBContainer);
            this.TestArticleBar.Controls.Add(this.label7);
            this.TestArticleBar.Controls.Add(this.label6);
            this.TestArticleBar.Controls.Add(this.label1);
            this.TestArticleBar.Controls.Add(this.TestArticleInfo);
            this.TestArticleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TestArticleBar.Location = new System.Drawing.Point(0, 0);
            this.TestArticleBar.Name = "TestArticleBar";
            this.TestArticleBar.Size = new System.Drawing.Size(1157, 100);
            this.TestArticleBar.TabIndex = 0;
            this.TestArticleBar.Visible = false;
            // 
            // TestHBContainer
            // 
            this.TestHBContainer.BackColor = System.Drawing.Color.Transparent;
            this.TestHBContainer.Controls.Add(this.TestHBContainer2);
            this.TestHBContainer.Dock = System.Windows.Forms.DockStyle.Right;
            this.TestHBContainer.Location = new System.Drawing.Point(831, 0);
            this.TestHBContainer.Name = "TestHBContainer";
            this.TestHBContainer.Size = new System.Drawing.Size(326, 100);
            this.TestHBContainer.TabIndex = 6;
            // 
            // TestHBContainer2
            // 
            this.TestHBContainer2.Controls.Add(this.label9);
            this.TestHBContainer2.Controls.Add(this.label8);
            this.TestHBContainer2.Dock = System.Windows.Forms.DockStyle.Top;
            this.TestHBContainer2.Location = new System.Drawing.Point(0, 0);
            this.TestHBContainer2.Name = "TestHBContainer2";
            this.TestHBContainer2.Size = new System.Drawing.Size(326, 33);
            this.TestHBContainer2.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(61, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(256, 30);
            this.label9.TabIndex = 2;
            this.label9.Text = "我是HB我是HB我是HB";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label8.Dock = System.Windows.Forms.DockStyle.Left;
            this.label8.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 30);
            this.label8.TabIndex = 1;
            this.label8.Text = "看板";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(76, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(589, 30);
            this.label7.TabIndex = 5;
            this.label7.Text = "我是標題我是標題我是標題我是標題我是標題我是標題";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(76, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(205, 30);
            this.label6.TabIndex = 4;
            this.label6.Text = "我是時間我是時間";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(76, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(301, 30);
            this.label1.TabIndex = 3;
            this.label1.Text = "我是作者我是作者我是作者";
            // 
            // TestArticleInfo
            // 
            this.TestArticleInfo.BackColor = System.Drawing.SystemColors.ControlDark;
            this.TestArticleInfo.Controls.Add(this.label5);
            this.TestArticleInfo.Controls.Add(this.標題);
            this.TestArticleInfo.Controls.Add(this.Author);
            this.TestArticleInfo.Dock = System.Windows.Forms.DockStyle.Left;
            this.TestArticleInfo.Location = new System.Drawing.Point(0, 0);
            this.TestArticleInfo.Name = "TestArticleInfo";
            this.TestArticleInfo.Size = new System.Drawing.Size(70, 100);
            this.TestArticleInfo.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(6, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 30);
            this.label5.TabIndex = 2;
            this.label5.Text = "時間";
            // 
            // 標題
            // 
            this.標題.AutoSize = true;
            this.標題.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.標題.ForeColor = System.Drawing.Color.Navy;
            this.標題.Location = new System.Drawing.Point(6, 33);
            this.標題.Name = "標題";
            this.標題.Size = new System.Drawing.Size(61, 30);
            this.標題.TabIndex = 1;
            this.標題.Text = "標題";
            // 
            // Author
            // 
            this.Author.AutoSize = true;
            this.Author.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Author.ForeColor = System.Drawing.Color.Navy;
            this.Author.Location = new System.Drawing.Point(6, 3);
            this.Author.Name = "Author";
            this.Author.Size = new System.Drawing.Size(61, 30);
            this.Author.TabIndex = 0;
            this.Author.Text = "作者";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.WriteArticle,
            this.writecomment});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(123, 48);
            // 
            // WriteArticle
            // 
            this.WriteArticle.Name = "WriteArticle";
            this.WriteArticle.Size = new System.Drawing.Size(122, 22);
            this.WriteArticle.Text = "撰寫文章";
            this.WriteArticle.Click += new System.EventHandler(this.WriteArticle_Click);
            // 
            // writecomment
            // 
            this.writecomment.Enabled = false;
            this.writecomment.Name = "writecomment";
            this.writecomment.Size = new System.Drawing.Size(122, 22);
            this.writecomment.Text = "新增留言";
            this.writecomment.Click += new System.EventHandler(this.writecomment_Click);
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1203, 599);
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Controls.Add(this.TestArticlePanel);
            this.Controls.Add(this.TestInnerPanel);
            this.Controls.Add(this.HomePanel);
            this.Controls.Add(this.NavPanel);
            this.DisplayHeader = false;
            this.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "MainPage";
            this.Padding = new System.Windows.Forms.Padding(23, 50, 23, 27);
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.Load += new System.EventHandler(this.MainPage_Load);
            this.NavPanel.ResumeLayout(false);
            this.BreadCrumb.ResumeLayout(false);
            this.next_previous_panel.ResumeLayout(false);
            this.TestInnerPanel.ResumeLayout(false);
            this.TestBox.ResumeLayout(false);
            this.TestBox.PerformLayout();
            this.TestArticlePanel.ResumeLayout(false);
            this.TestArticleBar.ResumeLayout(false);
            this.TestArticleBar.PerformLayout();
            this.TestHBContainer.ResumeLayout(false);
            this.TestHBContainer2.ResumeLayout(false);
            this.TestHBContainer2.PerformLayout();
            this.TestArticleInfo.ResumeLayout(false);
            this.TestArticleInfo.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button personalinfo;
        private System.Windows.Forms.Panel HomePanel;
        private System.Windows.Forms.Button Previous;
        private System.Windows.Forms.Button Next;
        private System.Windows.Forms.Panel NavPanel;
        private System.Windows.Forms.Panel TestInnerPanel;
        private System.Windows.Forms.GroupBox TestBox;
        private System.Windows.Forms.Panel next_previous_panel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Frontest;
        private System.Windows.Forms.Button Behindest;
        private System.Windows.Forms.Panel TestArticlePanel;
        private System.Windows.Forms.Panel TestArticleBar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel TestArticleInfo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label 標題;
        private System.Windows.Forms.Label Author;
        private System.Windows.Forms.Panel TestHBContainer;
        private System.Windows.Forms.Panel TestHBContainer2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel BreadCrumb;
        private MetroFramework.Components.MetroStyleExtender metroStyleExtender1;
        private System.Windows.Forms.Label Home;
        private System.Windows.Forms.Label HBcrumb;
        private System.Windows.Forms.Panel main_artecle;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem WriteArticle;
        private System.Windows.Forms.ToolStripMenuItem writecomment;
    }
}