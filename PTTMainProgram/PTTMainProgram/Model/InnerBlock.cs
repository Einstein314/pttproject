﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTTMainProgram.Model
{
    public class InnerBlock
    {
        public int IB_Id { get; set; }
        public int HB_Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string date { get; set; }
    }
}
