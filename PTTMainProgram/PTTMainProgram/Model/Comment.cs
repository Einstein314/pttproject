﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTTMainProgram.Model
{
    public class Comment
    {
        public int C_Id { get; set; }
        public int U_Id { get; set; }
        public int A_Id { get; set; }
        public string username { get; set; }
        public string type { get; set; }
        public string comment { get; set; }
        public string IpDateTime { get; set; }
    }
}
