﻿using SQLTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PTTMainProgram.Model
{
    public class Article
    {
        public int A_Id { get; set; }
        public int U_Id { get; set; }
        public int IB_Id { get; set; }
        public int HB_Id { get; set; }
        public string date { get; set; }
        public string author { get; set; }
        public string title { get; set; }
        public string words { get; set; }

        public List<A_pic> GetA_pic()
        {
            BaseDB db = new BaseDB();
            List<A_pic> list = db.NewQuery<A_pic>($"select pic_Id,A_Id,pic_link from A_pic where A_Id={this.A_Id}");
            return list;
        }
         
        //正則表達式
        public List<string> GetArticleContentURL(string content)
        {
            List<string> urllist = new List<string>();
            string patterns = "(http://|https://)[A-Za-z0-9/.]+(.jpg|.png|.bmp)";
            if (Regex.IsMatch(content,patterns))
            {
                MatchCollection matches = Regex.Matches(content, patterns);
                foreach(Match m in matches)
                {
                    urllist.Add(m.Value);
                }
            }
            return urllist;
        }
    }  
}
