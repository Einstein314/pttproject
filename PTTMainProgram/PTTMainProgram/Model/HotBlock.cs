﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTTMainProgram.Model
{
    public class HotBlock
    {
        public int HB_Id { get; set; }
        public string Name_E { get; set; }
        public int number { get; set; }
        public string Name_C { get; set; }
        public string discription { get; set; }
    }
}
