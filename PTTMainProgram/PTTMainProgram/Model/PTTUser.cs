﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTTMainProgram.Model
{
    public class PTTUser
    {
        public int U_Id { get; set; }
        public string name { get; set; }
        public int score { get; set; }
        public int signindate { get; set; }
        public string lastdate { get; set; }
    }
}
