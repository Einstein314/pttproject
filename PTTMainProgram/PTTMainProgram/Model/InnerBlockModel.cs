﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PTTMainProgram.Model
{
    public class InnerBlockModel
    {
        public int IB_Id { get; set; }
        public int HB_Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string date { get; set; }
        public int count { get; set; }
    }
}