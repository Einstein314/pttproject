﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;
using PTTMainProgram.Model;
using SQLTool;

namespace PTTBlockModule
{
    public class InnerBlockModule : ConnectSQL
    {
        BaseDB db;
        public InnerBlockModule()
        {
            this.db = new BaseDB();
        }

        public List<InnerBlockModel> GetInnerBlockInfo(int hbid, int position, int quantity)
        {
            //當目前最大筆數 = 實際筆數時 offset 前 position-10筆 
            /*
             * if position=1080 quantity=1071 (position>quantity)
             * 需offset前1070筆 --> position-10
             * 
             * if position=1070 quantity=1070 (position==quantity)
             * 需offset前1060筆 --> position-10
             */
            if(position != 0)
                position = position - 10;
            
            List<InnerBlockModel> list  = db.NewQuery<InnerBlockModel>($@"
                  SELECT 
                   [IB_Id]
                  ,[Title]
                  ,[Author]
                  ,[date]
                  ,[HB_Id]
              FROM [PTT_System].[dbo].[InnerBlock]
              where HB_Id= {hbid} 
              ORDER BY IB_Id asc
              OFFSET {position} ROWS
              FETCH NEXT 10 ROWS ONLY").ToList();

            return list;
        }

        public int GetInnerBlockQuantity(int hbid)        {            InnerBlockModel block = db.NewQuery<InnerBlockModel>($"select COUNT(*) as count from InnerBlock where HB_Id={hbid}").FirstOrDefault();
            return block.count;
                    }

        public int GetFirstIB_Id(int hbid)
        {
            using (ConnectSQL connect = new ConnectSQL())
            {
                string sql = $"select TOP 1 IB_Id from InnerBlock where HB_Id={hbid}";
                SqlCommand command = connect.createSqlCommand(sql);
                SqlDataReader reader = command.ExecuteReader();
                reader.Read();

                return Int32.Parse(reader["IB_Id"].ToString());
            }
        }
    }
}
