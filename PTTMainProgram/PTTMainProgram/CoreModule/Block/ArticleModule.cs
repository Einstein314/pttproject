﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;
using PTTMainProgram.Model;
using SQLTool;

namespace PTTBlockModule
{
    public class ArticleModule : ConnectSQL
    {
        BaseDB db;
        public ArticleModule()
        {
            db = new BaseDB();
        }

        public Article GetArticle(int ibid)
        {
            Article article = db.NewQuery<Article>($"select U_Id,A_Id,HB_Id,IB_Id,date,author,title,words from Article where IB_Id={ibid}").FirstOrDefault();
            return article;
        }

        public string GetHBType(int ibid)
        {
            using (HotBlockService module = new HotBlockService())
            {
                int hbid = db.NewQuery<Article>($"select HB_Id from Article where IB_Id = {ibid}").FirstOrDefault().HB_Id;
                return module.GetHotBlock(hbid).Name_E;
            }
        }     
    }
}
