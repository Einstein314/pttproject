﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;
using PTTMainProgram.Model;
using SQLTool;

namespace PTTBlockModule 
{
    public class CommentModule: ConnectSQL
    {
        BaseDB dB;
        public CommentModule()
        {
            dB = new BaseDB();
        }
        public List<Comment> GetComment(int aid)
        {
            List<Comment> comments = new List<Comment>();
            comments = dB.NewQuery<Comment>($"select * from Comment where A_Id = {aid}");
            return comments;           
        }
    }
}
