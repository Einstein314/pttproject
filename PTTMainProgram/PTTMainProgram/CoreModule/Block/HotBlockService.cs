﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;
using PTTMainProgram.Model;
using SQLTool;

namespace PTTBlockModule
{
    public class HotBlockService : ConnectSQL
    {
        BaseDB db;
        public HotBlockService()
        {
            db = new BaseDB();
        }
        
        public HotBlock GetHotBlock(int hbid)
        {
            HotBlock hotBlock = db.NewQuery<HotBlock>($"select Name_E,number,Name_C,discription from HotBlock where HB_Id = {hbid}").FirstOrDefault();
            return hotBlock;
        }

        

    }
}
