﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PTTconnectSQL;
using PTTMainProgram.Model;
using SQLTool;

namespace PTTScore
{
    public class ScoreService
    {
        BaseDB db;
        public ScoreService()
        {
            db = new BaseDB();
        }

        public void SetLoginDate(int id)
        {
            PTTUser user = db.NewQuery<PTTUser>($"select * from PTTUser where U_Id={id}").FirstOrDefault();
            if (DateTime.Compare(DateTime.Parse(user.lastdate), DateTime.Today) != 0)
            {
                user.score++;
                user.signindate++;
            }
            user.lastdate = DateTime.Now.ToShortDateString();
            this.db.Update(user);
        }
    }
}
