﻿using PTTMainProgram.Model;
using SQLTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTTMainProgram.CoreModule.Create
{
    public class CreateArticleModule
    {
        BaseDB db;
        public CreateArticleModule()
        {
            this.db = new BaseDB();
        }

        public List<HotBlock> getArticle()
        {
            string sql = "select * from HotBlock";
            return this.db.NewQuery<HotBlock>(sql);
        }

        public void writeArticle(int myblock, string mytitle, string username, string date, string datetime, string mycontent, int myuid)
        {
            InnerBlock innerBlock = new InnerBlock();          
            innerBlock.HB_Id = myblock;
            innerBlock.Title = mytitle;
            innerBlock.Author = username;
            innerBlock.date = date;
            db.Insert<InnerBlock>(innerBlock);

            InnerBlock will_binding_innerblock = new InnerBlock();
            string sql = $@"select * from InnerBlock where 
                            HB_Id={myblock} and Title='{mytitle}' and Author='{username}' and date='{date}'";
            will_binding_innerblock = db.NewQuery<InnerBlock>(sql).FirstOrDefault();

            //binding Article
            Article article = new Article();           
            article.HB_Id = will_binding_innerblock.HB_Id;
            article.U_Id = myuid;
            article.IB_Id = will_binding_innerblock.IB_Id;
            article.author = will_binding_innerblock.Author;
            article.date = datetime;
            article.title = will_binding_innerblock.Title;
            article.words = mycontent+"\r\n--";

            db.Insert<Article>(article);

            string sql2 = $"select * from Article where IB_Id = '{article.IB_Id}' and U_Id = '{article.U_Id}' ";
            Article writtenArticle = db.NewQuery<Article>(sql2).FirstOrDefault();
            List<string> pic_links = writtenArticle.GetArticleContentURL(writtenArticle.words);

            foreach(string pic_link in pic_links)
            {
                insertArticlePicture(writtenArticle.A_Id, pic_link);
            }
            
        }

        public  void insertArticlePicture(int A_Id, string pic_link)
        {
            A_pic a_Pic = new A_pic();
            a_Pic.A_Id = A_Id;
            a_Pic.pic_link = pic_link;

            db.Insert<A_pic>(a_Pic);
        }

    }
}
