﻿using PTTMainProgram.Model;
using SQLTool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTTMainProgram.CoreModule.Create
{
    public class CreateCommentModule
    {
        BaseDB db;
        Comment comment;
        public CreateCommentModule()
        {
            db = new BaseDB();
        }

        public void writeComment(Comment comment)
        {
            this.comment = comment;
            db.Insert<Comment>(comment);
        }
    }
}
