﻿using Newtonsoft.Json;
using PTTMainProgram.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PTTMainProgram.CoreModule.Utility
{
    public class Imgur
    {
        public string token;
        public Imgur()
        {
            //可能會過期 去imgur用開發人員工具拿sessionKey再去postman抓AccessToken
            token = ConfigurationManager.AppSettings["token"];
        }    

        public ImgurModel upload(string filePath)
        {           
            //建立 WebRequest 並指定目標的 uri
            WebRequest request = WebRequest.Create("https://api.imgur.com/3/image");
            request.Headers.Add(HttpRequestHeader.Authorization, $"Bearer {token}");
            request.Method = "POST";
            object postData = new { image = File.ReadAllBytes(filePath) };
            //將需 post 的資料內容轉為 stream 
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(postData);
                streamWriter.Write(json);
                streamWriter.Flush();
            }
            request.ContentType = "application/json; charset = utf-8"; 
            ImgurModel model;
            using (var httpResponse = (HttpWebResponse)request.GetResponse())
            {
                //使用 GetResponseStream 方法從 server 回應中取得資料，stream 必需被關閉
                //使用 stream.close 就可以直接關閉 WebResponse 及 stream，但同時使用 using 或是關閉兩者並不會造成錯誤，養成習慣遇到其他情境時就比較不會出錯
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    model = JsonConvert.DeserializeObject<ImgurModel>(result);
                    Console.WriteLine(result);
                }
            }           
            return model;
        }
    }   
}
