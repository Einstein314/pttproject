﻿using PTTMainProgram.CoreModule.Utility;
using PTTMainProgram.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PTTMainProgram.Utility
{
    public class UploadImage
    {
        public static string uploadimg()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            string filePath = "";
            openFileDialog.InitialDirectory = @"C:\Users\Einstein\Desktop";
            openFileDialog.Filter = "圖片檔(*.jpg)|*.jpg|圖片檔(*.png)|*.png";

            string link;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                filePath = openFileDialog.FileName;
                Imgur imgur = new Imgur();
                ImgurModel model = imgur.upload(filePath);
                link = "\r\n "+model.data.link+" \r\n";              
            }
            else
            {
                link = "";
            }
            return link;
        }
    }
}
