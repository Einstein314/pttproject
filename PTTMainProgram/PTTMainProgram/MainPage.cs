﻿using PTTBlockModule;
using PTTMainProgram.Model;
using PTTScore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PTTMainProgram
{
    public partial class MainPage : MetroFramework.Forms.MetroForm
    {
        private int id;
        private string name;
        private int HotBlockId;
        List<GroupBox> boxes = new List<GroupBox>();

        public Label createLabel(string text, Size size, Point point, float fontsize = 15F, string familyname = "微軟正黑體", Color? c = null)
        {
            Label HBlabel = new Label();
            c = c == null ? Color.White : c;

            HBlabel.Font = new System.Drawing.Font(familyname, fontsize, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            HBlabel.ForeColor = (Color)c;
            HBlabel.Text = text;
            HBlabel.Size = size;
            HBlabel.Location = point;

            return HBlabel;
        }
        public Label createLabel(string text, Point point, float fontsize = 15F, string familyname = "微軟正黑體", Color? c = null)
        {
            Label HBlabel = new Label();
            c = c == null ? Color.White : c;

            HBlabel.Font = new System.Drawing.Font(familyname, fontsize, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            HBlabel.ForeColor = (Color)c;
            HBlabel.Text = text;
            HBlabel.AutoSize = true;
            HBlabel.Location = point;

            return HBlabel;
        }
        public LinkLabel createLinkLabel(string text, Point point, float fontsize = 15F, string familyname = "微軟正黑體", Color? c = null)
        {
            LinkLabel HBlabel = new LinkLabel();
            c = c == null ? Color.White : c;

            HBlabel.Font = new System.Drawing.Font(familyname, fontsize, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            HBlabel.ForeColor = (Color)c;
            HBlabel.Text = text;
            HBlabel.AutoSize = true;
            HBlabel.Location = point;

            return HBlabel;
        }

        public MainPage(int id, string name)
        {
            using (HotBlockService hbmodule = new HotBlockService())
            {
                //動態生成HB

                for (int i = 1; i <= 128; i++)
                {
                    GroupBox box = new GroupBox();
                    Label Name_E = createLabel(hbmodule.GetHotBlock(i).Name_E, new Size(125, 25), new Point(6, 17));
                    Label number = createLabel(hbmodule.GetHotBlock(i).number.ToString(), new Size(108, 25), new Point(312, 17));
                    Label Name_C = createLabel(hbmodule.GetHotBlock(i).Name_C, new Size(126, 25), new Point(437, 17));
                    Label description = createLabel(hbmodule.GetHotBlock(i).discription, new Size(379, 25), new Point(583, 17));

                    box.Name = hbmodule.GetHotBlock(i).Name_E;
                    box.Tag = i; //hbid
                    box.Size = new System.Drawing.Size(977, 53);
                    box.Location = new System.Drawing.Point(26, 40 + 105 * (i - 1) + (i));
                    box.MouseEnter += new EventHandler(BoxMouseEnter);
                    box.MouseLeave += new EventHandler(BoxMouseLeave);
                    box.MouseClick += new MouseEventHandler(BoxMouseClick);

                    box.Controls.Add(Name_E);
                    box.Controls.Add(number);
                    box.Controls.Add(Name_C);
                    box.Controls.Add(description);

                    this.Controls.Add(box);
                    boxes.Add(box);
                }
            }

            InitializeComponent(); //初始化完才有Panel
            this.id = id;
            this.name = name;

            foreach (var box in boxes) //加到Panel
            {
                HomePanel.Controls.Add(box);
            }           
        }   
        private void MainPage_Load(object sender, EventArgs e)
        {
            ScoreService module = new ScoreService();
            module.SetLoginDate(id);         
        }

        //動態生成InnerBlock
        Panel InnerPanel;
        int quantity = 0;
        private void CreateInnerPanel(int hbid)
        {
            using (InnerBlockModule module = new InnerBlockModule()) //InnerPanel 用動態生成 分頁數
            {
                next_previous_panel.Visible = true;
                next_previous_panel.Enabled = true;

                quantity = module.GetInnerBlockQuantity(hbid);
                IfNextOrPrevious(quantity);

                InnerPanel = new Panel();
                InnerPanel.AutoScroll = true;
                InnerPanel.BackColor = System.Drawing.Color.Transparent;
                InnerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
                InnerPanel.Location = new System.Drawing.Point(23, 95);
                InnerPanel.Name = hbid.ToString();
                InnerPanel.RightToLeft = System.Windows.Forms.RightToLeft.No;
                InnerPanel.Size = new System.Drawing.Size(1042, 495);
                InnerPanel.Visible = true;
                this.Controls.Add(InnerPanel);

                List<GroupBox> list = new List<GroupBox>();
                List<InnerBlockModel> innerBlockmodel = module.GetInnerBlockInfo(hbid, position, quantity);
                for (int i = 0; i < innerBlockmodel.Count; i++)//ftibid用first取代
                {
                    GroupBox innerbox = new GroupBox();
                    Label Title = createLabel(innerBlockmodel[i].Title, new Size(877, 30), new Point(6, 19), 18F);
                    Label Author = createLabel(innerBlockmodel[i].Author, new Size(86, 24), new Point(7, 49), 14.25F);
                    Label Date = createLabel(innerBlockmodel[i].date, new Size(86, 24), new Point(950, 49), 14.25F);

                    innerbox.Location = new System.Drawing.Point(0, 67 + (85 * (i)) + (2 * (i)));
                    innerbox.Size = new System.Drawing.Size(1042, 85);

                    Title.Tag = innerBlockmodel[i].IB_Id;
                    Title.Cursor = System.Windows.Forms.Cursors.Hand;
                    Title.MouseEnter += new EventHandler(TitleMouseEnter);
                    Title.MouseLeave += new EventHandler(TitleMouseLeave);
                    Title.MouseClick += new MouseEventHandler(TitleMouseClick); ;


                    innerbox.Controls.Add(Title);
                    innerbox.Controls.Add(Author);
                    innerbox.Controls.Add(Date);

                    this.Controls.Add(innerbox);
                    InnerPanel.Controls.Add(innerbox);
                    list.Add(innerbox);
                }
            }
        }

        //IB點擊進入文章 文章抓取
        FlowLayoutPanel main_article_panel = new FlowLayoutPanel();
        Article thisarticle;
        private void TitleMouseClick(object sender, MouseEventArgs e)
        {
            writecomment.Enabled = true;
            using (ArticleModule module = new ArticleModule())
            {                
                InnerPanel.Visible = false; // InnerBlock隱藏 保留當前頁面
                next_previous_panel.Visible = false; //上下頁隱藏
                next_previous_panel.Enabled = false;
                Label titlelabel = (Label)sender;
                int ibid = (int)titlelabel.Tag;

                thisarticle = module.GetArticle(ibid);
                //第一區塊-標題資訊--------------------------------------------------

                CreateArticleBar(main_article_panel,thisarticle, module.GetHBType(ibid));

                //第二區塊-文章--------------------------------------------------

                //main_article_panel.BackColor = Color.White;
                main_article_panel.Width = this.Width;
                main_article_panel.HorizontalScroll.Maximum = 0;
                main_article_panel.AutoScroll = false;
                main_article_panel.VerticalScroll.Visible = false;
                main_article_panel.AutoScroll = true;
                main_article_panel.Dock = System.Windows.Forms.DockStyle.Fill;
                main_article_panel.Padding = new Padding(0, NavPanel.Height, 0, 0);
                main_article_panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;

                
                this.Controls.Add(main_article_panel);
                //Console.WriteLine(thisarticle.title);
                CommentModule commentModule = new CommentModule();
                List<Comment> comments = commentModule.GetComment(thisarticle.A_Id);
       
                ShowArticleContent(main_article_panel, thisarticle, comments);           
            }           
            HBcrumb.Enabled = true; //當文章出現時才可以按回HB
        }

        private void CreateArticleBar(FlowLayoutPanel main, Article article , String blockName)
        {

            Panel ArticleBar = new Panel();
            Panel HBContainer = new Panel();
            Panel HBContainer2 = new Panel();

            Label HBLabel = createLabel(blockName, new Size(256, 30), new Point(61, 0));
            Label Board = createLabel("看板", new Size(589, 30), new Point(76, 33), 18F, c: Color.Navy);
            Label Title = createLabel(article.title, new Size(61, 30), new Point(76, 33), 18F);
            Label Time = createLabel(article.date, new Size(205, 30), new Point(76, 63), 18F);
            Label Author = createLabel(article.author, new Size(301, 30), new Point(76, 3), 18F);
            Panel ArticleInfo = new Panel();
            Label TM = createLabel("時間", new Size(61, 30), new Point(6, 63), 18F, c: Color.Navy);
            Label TT = createLabel("標題", new Size(61, 30), new Point(6, 33), 18F, c: Color.Navy);
            Label AT = createLabel("作者", new Size(61, 30), new Point(6, 3), 18F, c: Color.Navy);

            ArticleBar.BackColor = System.Drawing.Color.Navy;
            ArticleBar.Controls.Add(HBContainer);
            ArticleBar.Controls.Add(Time);
            ArticleBar.Controls.Add(Title);
            ArticleBar.Controls.Add(Author);
            ArticleBar.Controls.Add(ArticleInfo);
            ArticleBar.Dock = System.Windows.Forms.DockStyle.Top;
            ArticleBar.Location = new System.Drawing.Point(0, 0);     
            ArticleBar.Size = new System.Drawing.Size(this.Width, 100);

            HBContainer.BackColor = System.Drawing.Color.Transparent;
            HBContainer.Controls.Add(HBContainer2);
            HBContainer.Dock = System.Windows.Forms.DockStyle.Right;
            HBContainer.Location = new System.Drawing.Point(716, 0);
            HBContainer.Size = new System.Drawing.Size(326, 100);

            HBContainer2.Controls.Add(HBLabel);
            HBContainer2.Controls.Add(Board);
            HBContainer2.Dock = System.Windows.Forms.DockStyle.Top;
            HBContainer2.Location = new System.Drawing.Point(0, 0);
            HBContainer2.Size = new System.Drawing.Size(326, 33);

            HBLabel.AutoSize = true;
            HBLabel.BackColor = System.Drawing.Color.Transparent;
            HBLabel.Dock = System.Windows.Forms.DockStyle.Left;

            Board.AutoSize = true;
            Board.BackColor = System.Drawing.SystemColors.ControlDark;
            Board.Dock = System.Windows.Forms.DockStyle.Left;

            Title.AutoSize = true;

            Time.AutoSize = true;

            Author.AutoSize = true;

            ArticleInfo.BackColor = System.Drawing.SystemColors.ControlDark;
            ArticleInfo.Controls.Add(TM);
            ArticleInfo.Controls.Add(TT);
            ArticleInfo.Controls.Add(AT);
            ArticleInfo.Dock = System.Windows.Forms.DockStyle.Left;
            ArticleInfo.Location = new System.Drawing.Point(0, 0);;
            ArticleInfo.Size = new System.Drawing.Size(70, 100);

            TM.AutoSize = true;        
            TT.AutoSize = true;
            AT.AutoSize = true;

            main.Controls.Add(ArticleBar);
            
        }

        private void ShowArticleContent(FlowLayoutPanel main, Article article, List<Comment> comments)
        {        
            List<Panel> list = new List<Panel>();
            for(int i=0; i<main.Controls.Count; i++)
            {
                if(main.Controls[i] is Panel)
                {
                    list.Add((Panel)main.Controls[i]);
                }
            }
            for (int i = 0; i < main.Controls.Count; i++)
            {        
               
                if (main.Controls[i] is FlowLayoutPanel) //your condition here
                {
                    main.Controls.Remove(main.Controls[i]);
                    i--;                  
                }
            }
            if(list.Count>1)
            {
                for(int i=0;i<list.Count-1;i++)
                {
                    main.Controls.Remove(list[i]);
                }
            }
            var contentss =  article.words.Split(new String[] { "\r\n" }, StringSplitOptions.None).ToArray();

            if (article.U_Id == 0)
                contentss = contentss.Skip(4).ToArray();
            String content = String.Join("\r\n",contentss);


            //string[] eachpara = article.words.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            //string[] links = new string[eachpara.Length];
            //int record = 0;
            //for (int i = 0; i < eachpara.Length; i++)
            //{
            //    if (eachpara[i].Contains("https"))
            //    {
            //        links[record] = eachpara[i];
            //        record++;
            //    }
            //}

            List<string> urls = article.GetArticleContentURL(article.words);
            var imgs = article.GetA_pic().Select(x=>x.pic_link).ToArray();
            foreach (var u in urls)
            {
                if (content.Contains(u))
                {
                    content = content.Replace(u, "i'm a pic_link-pic_link-pic_link");
                }
            }
            string[] contents = content.Split(new string[] { "i'm a pic_link-pic_link-pic_link" }, StringSplitOptions.None);
           

            for (int i = 0; i < urls.Count; i++)
            {
                FlowLayoutPanel panel_text = new FlowLayoutPanel();     
                panel_text.Width = main.Width;

                Label label = createLabel(contents[i], new Point(0, 500), 18F, c: Color.White);
                FlowLayoutPanel linkpanel = new FlowLayoutPanel();

                LinkLabel linkLabel = createLinkLabel(urls[i], new Point(0, 500), 18F, c: Color.White);
                linkLabel.Click += openURL;

         
                linkpanel.Controls.Add(linkLabel);
                linkpanel.Width = main.Width;
                linkpanel.Height = linkLabel.Height;
   

                panel_text.Controls.Add(label);
                panel_text.Controls.Add(linkpanel);
                //Console.WriteLine(label.Height);
                FlowLayoutPanel panel_picture = new FlowLayoutPanel();

                panel_picture.Width = main.Width;
       
                PictureBox picture = new PictureBox();
                picture.SizeMode = PictureBoxSizeMode.StretchImage;

                try 
                {
                    //picture.LoadAsync(imgs[i]);  //非同步load較快
                    int img_height, img_width;
                    var img = Bitmap.FromStream(new MemoryStream(new WebClient().DownloadData(imgs[i])));  //用Bitmap取得照片 進而取得長寬
                    if (img.Width >= main.Width)
                    {
                        img = ResizeImage(img, img.Width / 2, img.Height/2); //等比例縮小
                                            
                    }                  
                    img_height = img.Height;
                    img_width = img.Width;
                    
                    
                    picture.Height = img_height;
                    picture.Width = img_width;
                   
                    panel_picture.Height = picture.Height;
                    picture.Image = img;
                    
                }
                catch //照片找不到
                {
                    Image notfound = Image.FromFile(@"C:\Users\Einstein\Source\Repos\PTTproject\PTTMainProgram\PTTMainProgram\bin\Debug\Resource\nosuchimage.png");
                    picture.Height = notfound.Height ;
                    picture.Width = notfound.Width ;
                    picture.Image = notfound;
                }
                              

               
                panel_picture.Controls.Add(picture);
                //Console.WriteLine(label.Height);
                main.Controls.Add(panel_text);
                //Console.WriteLine(label.Height);
                main.Controls.Add(panel_picture);               
                panel_text.Height = label.Height+linkpanel.Height;
               
            }
            FlowLayoutPanel lastpanel = new FlowLayoutPanel();
            lastpanel.Width = main.Width;
            Label last = createLabel(contents.LastOrDefault(), new Point(0, 500), 18F, c: Color.White);
            lastpanel.Controls.Add(last);
            main.Controls.Add(lastpanel);
            lastpanel.Height = last.Height;
            //lastpanel.BackColor = Color.Yellow;

            FlowLayoutPanel comments_panel = new FlowLayoutPanel();
            comments_panel.Width = main.Width;
            comments_panel.AutoSize = true;
            if (comments.Count != 0)
            {         
                for (int i = 0; i < comments.Count; i++)
                {
                    FlowLayoutPanel comment_panel = new FlowLayoutPanel();
                    comment_panel.Width = main.Width;

                    Label type = createLabel(comments[i].type, new Point(950, 49), 14.25F);
                    if (!comments[i].type.Contains("推"))
                    {
                        type.ForeColor = Color.Red;
                    }
                    else
                    {
                        type.ForeColor = Color.Yellow;
                    }
                   

                    Label username = createLabel(comments[i].username, new Point(950, 49), 14.25F);
                    username.ForeColor = Color.Yellow;

                    Label comment = createLabel(comments[i].comment, new Point(950, 49), 14.25F);
                    comment.ForeColor = Color.Yellow;

                    comment_panel.Height = comment.Height;

                    int n = 0;                
                    FlowLayoutPanel comment_pic_panel = new FlowLayoutPanel();
                    comment_pic_panel.AutoSize = false;
                    if (comment.Text.Contains("https://i.imgur.com"))
                    {
                        n++;
                        //comment_pic_panel.Width = main.Width - type.Width - username.Width;
                        comment_pic_panel.Width = main.Width;

                        string[] filt_url = comment.Text.Split(new string[] {"https://"}, StringSplitOptions.RemoveEmptyEntries);
                        string com_img_url = "https://"+filt_url[1]; //取得照片url

                        FlowLayoutPanel linkpanel = new FlowLayoutPanel();

                        LinkLabel linkLabel = createLinkLabel(com_img_url, new Point(0, 500), 18F, c: Color.White);
                        linkLabel.Click += openURL;

                        linkpanel.Controls.Add(linkLabel);
                        linkpanel.Width = main.Width;
                        linkpanel.Height = linkLabel.Height;

                        var com_img = Bitmap.FromStream(new MemoryStream(new WebClient().DownloadData(com_img_url)));//  //用Bitmap取得照片 進而取得長寬
                        
                        int com_img_height;
                        int com_img_width;

                        PictureBox box = new PictureBox();
                        box.SizeMode = PictureBoxSizeMode.StretchImage;

                        if (com_img.Width >= main.Width)
                        {                          
                            var recom_img = ResizeImage(com_img, com_img.Width/2, com_img.Height/2);
                            com_img_width = recom_img.Width;
                            com_img_height = recom_img.Height;
                            box.Image = recom_img;

                        }
                        else
                        {
                            com_img_width = com_img.Width;
                            com_img_height = com_img.Height;
                            box.Image = com_img;
                        }
                                               
                        box.Height = com_img_height+20;                      
                        box.Width = com_img_width;
                        //Console.WriteLine(comment.Text.Substring(2));
                        //box.LoadAsync(comment.Text.Substring(2));

                        comment_pic_panel.Height = box.Height + linkpanel.Height;
                        comment_pic_panel.Controls.Add(linkpanel);
                        comment_pic_panel.Controls.Add(box);
                        //main.Controls.Add(comment_pic_panel);
                    }

                    Label IpDateTime = createLabel(comments[i].IpDateTime, new Point(950, 49), 14.25F);
                    IpDateTime.AutoSize = false;

                    IpDateTime.TextAlign = ContentAlignment.MiddleRight;

                    comment_panel.Controls.Add(type);
                    comment_panel.Controls.Add(username);
                    comment_panel.Controls.Add(comment);
                    comment_panel.Controls.Add(IpDateTime);
                    if (n > 0)
                    {
                        comment_panel.Controls.Add(comment_pic_panel);
                        comment_pic_panel.Margin = new Padding(type.Width + username.Width + 20, 20, 0, 20);
                        comment_panel.Height += comment_pic_panel.Height+(int)(comment_pic_panel.Height/8);
                    }

                    comments_panel.Controls.Add(comment_panel);
                    IpDateTime.Width = comment_panel.Width - type.Width - username.Width - comment.Width - 50;
                }
                comments_panel.Controls[comments_panel.Controls.Count - 1].Height += 50;                
            }
            main.Controls.Add(comments_panel);
        }

        private void openURL(object sender, EventArgs e)
        {
            LinkLabel linkLabel = (LinkLabel)sender;
            System.Diagnostics.Process.Start(linkLabel.Text);
        }

        //上下頁是否可按
        int pagenumber = 0;
        int position = 0;
        private void IfNextOrPrevious(int quantity)
        {
            Previous.Enabled = true;
            Next.Enabled = false;
            Behindest.Enabled = false;
            Frontest.Enabled = true;

            position = pagenumber * 10;
            //如果目前筆數>=總筆數 則不能按上一頁
            if (pagenumber!=0 && (position >= quantity))
            {
                Previous.Enabled = false;
                Next.Enabled = true;
                Behindest.Enabled = true;
                Frontest.Enabled = false;
            }
            else if (pagenumber == 0)
            {
                Previous.Enabled = true;
                Next.Enabled = false;
                Behindest.Enabled = false;
                Frontest.Enabled = true;
            }
            else
            {
                Previous.Enabled = true;
                Next.Enabled = true;
                Behindest.Enabled = true;
                Frontest.Enabled = true;
            }
        }

        //個人資料按鈕
        private void personalinfo_Click(object sender, EventArgs e)
        {
            PersonalInfo.createPersonalInfo(id).Show();
           
        }

        //Box-->HotBlock
        private void BoxMouseEnter(object sender, EventArgs e)
        {
            GroupBox thisbox = (GroupBox)sender;
            thisbox.BackColor = System.Drawing.SystemColors.Control;
        }

        private void BoxMouseLeave(object sender, EventArgs e)
        {
            GroupBox thisbox = (GroupBox)sender;
            thisbox.BackColor = System.Drawing.Color.Transparent;
        }

        private void BoxMouseClick(object sender, EventArgs e)
        {
            GroupBox box = (GroupBox)sender;
            HotBlockId = Int32.Parse(box.Tag.ToString());
            HomePanel.Visible = false;
            using (InnerBlockModule module = new InnerBlockModule())
            {
                quantity = module.GetInnerBlockQuantity(HotBlockId);
                CreateInnerPanel(HotBlockId); //第一次產生InnerPanel
            }
            HBcrumb.Visible = true;
            HBcrumb.Text = "> " + box.Name;
        }

        //Title-->InnerBlock------------------------------------------
        private void TitleMouseEnter(object sender, EventArgs e)
        {
            Label Tlabel = (Label)sender;
            Tlabel.BackColor = System.Drawing.Color.WhiteSmoke;
        }

        private void TitleMouseLeave(object sender, EventArgs e)
        {
            Label Tlabel = (Label)sender;
            Tlabel.BackColor = System.Drawing.Color.Transparent;
        }

        //IB上下業按鈕function
        void Previous_Click(object sender, EventArgs e)
        {
            pagenumber++;
            this.Controls.Remove(InnerPanel);
            CreateInnerPanel(HotBlockId);
        }

        private void Next_Click(object sender, EventArgs e)
        {
            this.Controls.Remove(InnerPanel);
            pagenumber--;
            CreateInnerPanel(HotBlockId);
        }

        private void Frontest_Click(object sender, EventArgs e)
        {
            int check = quantity % 10;
            if(check==0)
            {
                pagenumber = quantity / 10;
            }else  //(check >= (quantity / 10)) 因為不可能小於
            {
                pagenumber = quantity / 10 + 1; //若共1069筆 則應該要有107頁 1069/10=106 106+1=107
            }           
            this.Controls.Remove(InnerPanel);
            CreateInnerPanel(HotBlockId);
        }

        private void behindest_Click(object sender, EventArgs e)
        {
            pagenumber = 0;
            this.Controls.Remove(InnerPanel);
            CreateInnerPanel(HotBlockId);
        }

        //返回function-------------------------------------------------
        private void Home_Click(object sender, EventArgs e)
        {
            HomePanel.Visible = true;
            this.Controls.Remove(InnerPanel);
            this.Controls.Remove(main_article_panel);
            HBcrumb.Text = "";
            HBcrumb.Enabled = false;
            writecomment.Enabled = false;
            pagenumber = 0;
            position = 0;
        }

        private void HBcrumb_Click(object sender, EventArgs e)
        {
            this.Controls.Remove(main_article_panel);
            InnerPanel.Visible = true;
            HBcrumb.Enabled = false;
            next_previous_panel.Visible = true;
            next_previous_panel.Enabled = true;
            writecomment.Enabled = false;            
        }

        private void BackHomePanel(object sender, EventArgs e)
        {
            Panel panel = (Panel)sender;
            if (panel.Visible == true)
            {
                next_previous_panel.Visible = false;
            }
        }

        private void WriteArticle_Click(object sender, EventArgs e)
        {
            WriteArticle write = new WriteArticle(name, id);
            write.Show();        
        }

        private void writecomment_Click(object sender, EventArgs e)
        {
            WriteComment comment = new WriteComment(thisarticle, id);
            comment.Show();
        }

        public static Bitmap ResizeImage(Image image, int width, int height) //high quality resized image
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
    }
}
