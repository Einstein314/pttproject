﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PTTSignup;

namespace PTTMainProgram
{
    public partial class Signup : MetroFramework.Forms.MetroForm
    {
        private static Signup signup = null;
        public Signup()
        {
            InitializeComponent();
        }

        public static Signup createSignup() //獨體模式
        {
            if(signup == null || signup.IsDisposed)
            {
                signup = new Signup();
            }

            return signup;
        }

        private void Signup_Load(object sender, EventArgs e)
        {
            date.CustomFormat = "yyyy/MM/dd";
            date.Format = DateTimePickerFormat.Custom;
        }

        private void confirm_Click(object sender, EventArgs e)
        {
            using (SignupModule signup = new SignupModule())
            {
                
                if (signup.IsValidEmail(email.Text.ToString())) //email格式：登錄名@主機名.域名
                {
                    if(signup.Signup(name.Text.ToString(), email.Text.ToString(), password.Text.ToString(), date.Text.ToString()) == 1)
                    {
                        MessageBox.Show("註冊成功");
                    }else
                    {
                        MessageBox.Show("註冊失敗");
                    }
                }else
                {
                    MessageBox.Show("註冊失敗 email格式錯誤！");
                }              
            }
            //MessageBox.Show(date.Text.ToString());
        }

       
    }
}
