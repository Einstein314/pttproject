﻿namespace PTTMainProgram
{
    partial class WriteArticle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.which_hotblock = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.content = new System.Windows.Forms.TextBox();
            this.contextMenuStrip_insertpicinarticle = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.insert_pic = new System.Windows.Forms.ToolStripMenuItem();
            this.write = new System.Windows.Forms.Button();
            this.contextMenuStrip_insertpicinarticle.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(51, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "版塊：";
            // 
            // which_hotblock
            // 
            this.which_hotblock.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.which_hotblock.FormattingEnabled = true;
            this.which_hotblock.Location = new System.Drawing.Point(139, 97);
            this.which_hotblock.Name = "which_hotblock";
            this.which_hotblock.Size = new System.Drawing.Size(218, 23);
            this.which_hotblock.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(51, 152);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "主題：";
            // 
            // title
            // 
            this.title.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.title.Location = new System.Drawing.Point(139, 151);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(268, 25);
            this.title.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(51, 209);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "內文：";
            // 
            // content
            // 
            this.content.ContextMenuStrip = this.contextMenuStrip_insertpicinarticle;
            this.content.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.content.Location = new System.Drawing.Point(139, 210);
            this.content.Multiline = true;
            this.content.Name = "content";
            this.content.Size = new System.Drawing.Size(617, 338);
            this.content.TabIndex = 5;
            // 
            // contextMenuStrip_insertpicinarticle
            // 
            this.contextMenuStrip_insertpicinarticle.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insert_pic});
            this.contextMenuStrip_insertpicinarticle.Name = "contextMenuStrip_insertpicinarticle";
            this.contextMenuStrip_insertpicinarticle.Size = new System.Drawing.Size(181, 48);
            // 
            // insert_pic
            // 
            this.insert_pic.Name = "insert_pic";
            this.insert_pic.Size = new System.Drawing.Size(180, 22);
            this.insert_pic.Text = "插入圖片";
            this.insert_pic.Click += new System.EventHandler(this.insert_pic_Click);
            // 
            // write
            // 
            this.write.Location = new System.Drawing.Point(790, 512);
            this.write.Name = "write";
            this.write.Size = new System.Drawing.Size(81, 36);
            this.write.TabIndex = 6;
            this.write.Text = "撰寫";
            this.write.UseVisualStyleBackColor = true;
            this.write.Click += new System.EventHandler(this.write_Click);
            // 
            // WriteArticle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 627);
            this.Controls.Add(this.write);
            this.Controls.Add(this.content);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.title);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.which_hotblock);
            this.Controls.Add(this.label1);
            this.Name = "WriteArticle";
            this.Text = "撰寫文章";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.contextMenuStrip_insertpicinarticle.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox which_hotblock;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox title;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox content;
        private System.Windows.Forms.Button write;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip_insertpicinarticle;
        private System.Windows.Forms.ToolStripMenuItem insert_pic;
    }
}