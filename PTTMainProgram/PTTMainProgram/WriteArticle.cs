﻿using PTTMainProgram.CoreModule.Create;
using PTTMainProgram.CoreModule.Utility;
using PTTMainProgram.Model;
using PTTMainProgram.Utility;
using PTTPersonalInfoModule;
using SQLTool;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PTTMainProgram
{
    public partial class WriteArticle : MetroFramework.Forms.MetroForm
    {
       
        CreateArticleModule create;
        string username;
        int uid;
        public WriteArticle(string username, int uid)
        {
            InitializeComponent();
            this.uid = uid;
            this.username = username;
            create = new CreateArticleModule();
            which_hotblock.DataSource = create.getArticle();
            which_hotblock.DisplayMember = "Name_C";
            which_hotblock.ValueMember = "HB_Id";
        }

        private void insert_pic_Click(object sender, EventArgs e)
        {
            content.Text += UploadImage.uploadimg();      
            
        }

        private void write_Click(object sender, EventArgs e)
        {            
            string myusername = username;
            int myblock = (int)which_hotblock.SelectedValue;
            string mytitle = title.Text;
            string mycontent = content.Text;
            string date = DateTime.Now.ToShortDateString().Substring(5); //yyyy/MM/dd
            string datetime = DateTime.Now.ToString("ddd MMM  d HH:mm:ss yyyy", CultureInfo.CreateSpecificCulture("en-GB"));// ddd MMM d H:mm:ss yyyy, 英文格式
            int myuid = uid;                       

            Console.WriteLine(myusername+" "+ date);
            create.writeArticle(myblock, mytitle, myusername, date, datetime, mycontent, myuid);       
            MessageBox.Show("撰寫成功!");
        }     
    }
}
