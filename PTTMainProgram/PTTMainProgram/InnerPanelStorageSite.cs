﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PTTMainProgram
{
    public class InnerPanelStorageSite
    {
        public int hbid { get; set; }
        public int entirenumber { get; set; }
        public int firstibid { get; set; }
        public List<GroupBox> list { get; set; }
    }
}
