﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PTTconnectSQL;
using PTTMainProgram.Model;
using PTTSignin;
using SQLTool;

namespace PTTMainProgram
{
    public partial class Form1 : MetroFramework.Forms.MetroForm
    {
        public Form1()
        {
            InitializeComponent();           
        }

        private void signup_Click(object sender, EventArgs e)
        {
            Signup.createSignup().Show();
            //Test SQL
            //ConnectSQL connectSQL = new ConnectSQL();
            //string sql = "select * from PTTUser";
            //SqlCommand command = connectSQL.createSqlCommand(sql);
            //SqlDataReader reader = command.ExecuteReader();
            //reader.Read();
            //MessageBox.Show(reader["name"].ToString());
        }

        private void signin_Click(object sender, EventArgs e)
        {
            BaseDB db = new BaseDB();
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add("email", email.Text.ToString());
            dict.Add("password", password.Text.ToString());
            PTTUser user = db.NewQuery<PTTUser>("select U_Id,name from PTTUser where email=@email and password=@password", dict).FirstOrDefault();
            if(user != null)
            {
                MessageBox.Show("登入成功！");
                //MainPage mainPage = new MainPage(user.U_Id);
                MainPage Main = new MainPage(user.U_Id, user.name);
                this.Hide();
                //mainPage.Show();
                Main.Show();
            }else
            {
                MessageBox.Show("登入失敗！");
            }
        }

        
    }
}
