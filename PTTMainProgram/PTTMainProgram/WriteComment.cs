﻿using PTTMainProgram.CoreModule.Create;
using PTTMainProgram.Model;
using PTTMainProgram.Utility;
using SQLTool;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PTTMainProgram
{
    public partial class WriteComment : MetroFramework.Forms.MetroForm
    {
        private Article currentarticle;
        private BaseDB db;
        private int userid;
        public WriteComment(Article currentarticle, int userid)
        {
            InitializeComponent();
            db = new BaseDB();
            this.currentarticle = currentarticle;
            this.userid = userid;
            HotBlock name = db.NewQuery<HotBlock>($"select Name_C,Name_E from HotBlock where HB_Id={currentarticle.HB_Id}").FirstOrDefault();
            articletype.Text = name.Name_E+" "+name.Name_C;
            articletitle.Text = currentarticle.title;
            
        }

        private void insert_pic_Click(object sender, EventArgs e)
        {
            commentcontent.Text += UploadImage.uploadimg();
            commentcontent.Enabled = false;
        }

        private void send_Click(object sender, EventArgs e)
        {
            Comment comment = new Comment();
            comment.type = commenttype.Text;
            comment.A_Id = currentarticle.A_Id;
            comment.U_Id = userid;
            comment.username = db.NewQuery<PTTUser>($"select name from PTTUser where U_Id={userid}").FirstOrDefault().name;
            comment.comment = ": " + commentcontent.Text;
           
            //取得IP
            // 取得本機名稱
            String strHostName = Dns.GetHostName();
            // 取得本機的 IpHostEntry 類別實體
            IPHostEntry iphostentry = Dns.GetHostByName(strHostName); 

            string ip = iphostentry.AddressList.FirstOrDefault().ToString();
            string datetime = DateTime.Now.ToString("MM/dd HH:mm");
            comment.IpDateTime = ip+" "+datetime;

            CreateCommentModule createComment = new CreateCommentModule();
            createComment.writeComment(comment);
            MessageBox.Show("留言成功!");
        }     
    }
}
