﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PTTconnectSQL;
using PTTPersonalInfoModule;

namespace PTTMainProgram
{
    public partial class PersonalInfo : MetroFramework.Forms.MetroForm
    {
        private static int myid;
        string pic;
        static PersonalInfo info = null;
        public PersonalInfo()
        {
            InitializeComponent();
            userpic.SizeMode = PictureBoxSizeMode.StretchImage; //使圖片彈性符合picbox大小
        }

        public static PersonalInfo createPersonalInfo(int id) //獨體模式
        {           
            if (info == null || info.IsDisposed)
            {
                myid = id;
                info = new PersonalInfo();
            }

            return info;
        }

        private void PersonalInfo_Load(object sender, EventArgs e)
        {
            using (PersonalInfoTool tool = new PersonalInfoTool())
            {
                SqlDataReader reader = tool.GetPersonalInfo(myid);
                reader.Read();                            
                userpic.Image = Image.FromFile(reader["U_pic"].ToString());
                pic = reader["U_pic"].ToString(); //初始化pic 以免沒更改圖片卻按下更改鍵
                name.Text = reader["name"].ToString();
                email.Text = reader["email"].ToString();
                password.Text = reader["password"].ToString();
                born.Text = reader["date"].ToString();
                score.Text = reader["score"].ToString();
                signinday.Text = reader["signindate"].ToString();
            }
        }

        private void alter_Click(object sender, EventArgs e)
        {
            using (PersonalInfoTool tool = new PersonalInfoTool())
            {                
                int alter = tool.SetPersonalInfo(myid, pic, name.Text, email.Text, password.Text, born.Text);
                if(alter>=1)
                {
                    MessageBox.Show("更改成功！");
                }else
                {
                    MessageBox.Show("更改失敗！");
                }
            }
        }

        private void chooseuserpic_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "請選擇圖片";
            dialog.InitialDirectory = ".\\";
            dialog.Filter = "Image files (*.jpg;*.png)|*.jpg;*.png"; //過濾 文件敘述|過濾條件
            
            if(dialog.ShowDialog() == DialogResult.OK)
            {
                pic = dialog.FileName;
                userpic.Image = Image.FromFile(pic);
            }else
            {
                MessageBox.Show("選取失敗！");                
            }
            
        }
    }
}
