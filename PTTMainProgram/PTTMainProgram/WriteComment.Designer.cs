﻿namespace PTTMainProgram
{
    partial class WriteComment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.articletitle = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.articletype = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.commentcontent = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.commenttype = new System.Windows.Forms.ComboBox();
            this.send = new System.Windows.Forms.Button();
            this.contextMenuStrip_insertpicincomment = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.insert_pic = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip_insertpicincomment.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.ForeColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(23, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "新增留言";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.ForeColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(61, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "文章標題：";
            // 
            // articletitle
            // 
            this.articletitle.AutoSize = true;
            this.articletitle.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.articletitle.ForeColor = System.Drawing.Color.Transparent;
            this.articletitle.Location = new System.Drawing.Point(172, 132);
            this.articletitle.Name = "articletitle";
            this.articletitle.Size = new System.Drawing.Size(86, 24);
            this.articletitle.TabIndex = 2;
            this.articletitle.Text = "文章標題";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.ForeColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(61, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 24);
            this.label3.TabIndex = 3;
            this.label3.Text = "文章類型：";
            // 
            // articletype
            // 
            this.articletype.AutoSize = true;
            this.articletype.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.articletype.ForeColor = System.Drawing.Color.Transparent;
            this.articletype.Location = new System.Drawing.Point(172, 89);
            this.articletype.Name = "articletype";
            this.articletype.Size = new System.Drawing.Size(86, 24);
            this.articletype.TabIndex = 4;
            this.articletype.Text = "文章類型";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.ForeColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(61, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 24);
            this.label4.TabIndex = 5;
            this.label4.Text = "留言內容：";
            // 
            // commentcontent
            // 
            this.commentcontent.ContextMenuStrip = this.contextMenuStrip_insertpicincomment;
            this.commentcontent.Location = new System.Drawing.Point(176, 220);
            this.commentcontent.Multiline = true;
            this.commentcontent.Name = "commentcontent";
            this.commentcontent.Size = new System.Drawing.Size(679, 124);
            this.commentcontent.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.ForeColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(61, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 24);
            this.label5.TabIndex = 7;
            this.label5.Text = "留言類型：";
            // 
            // commenttype
            // 
            this.commenttype.FormattingEnabled = true;
            this.commenttype.Items.AddRange(new object[] {
            "噓 ",
            "推 ",
            "→ "});
            this.commenttype.Location = new System.Drawing.Point(176, 179);
            this.commenttype.Name = "commenttype";
            this.commenttype.Size = new System.Drawing.Size(121, 20);
            this.commenttype.TabIndex = 8;
            this.commenttype.Text = "→ ";
            // 
            // send
            // 
            this.send.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.send.Location = new System.Drawing.Point(863, 388);
            this.send.Name = "send";
            this.send.Size = new System.Drawing.Size(93, 57);
            this.send.TabIndex = 9;
            this.send.Text = "留言";
            this.send.UseVisualStyleBackColor = true;
            this.send.Click += new System.EventHandler(this.send_Click);
            // 
            // contextMenuStrip_insertpicincomment
            // 
            this.contextMenuStrip_insertpicincomment.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insert_pic});
            this.contextMenuStrip_insertpicincomment.Name = "contextMenuStrip1";
            this.contextMenuStrip_insertpicincomment.Size = new System.Drawing.Size(123, 26);
            // 
            // insert_pic
            // 
            this.insert_pic.Name = "insert_pic";
            this.insert_pic.Size = new System.Drawing.Size(122, 22);
            this.insert_pic.Text = "插入圖片";
            this.insert_pic.Click += new System.EventHandler(this.insert_pic_Click);
            // 
            // WriteComment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 501);
            this.Controls.Add(this.send);
            this.Controls.Add(this.commenttype);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.commentcontent);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.articletype);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.articletitle);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "WriteComment";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.contextMenuStrip_insertpicincomment.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label articletitle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label articletype;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox commentcontent;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox commenttype;
        private System.Windows.Forms.Button send;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip_insertpicincomment;
        private System.Windows.Forms.ToolStripMenuItem insert_pic;
    }
}