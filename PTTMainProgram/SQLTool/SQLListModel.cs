﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLTool
{
    public class SQLListModel
    {
        public string Title { get; set; }
        public string date { get; set; }
        public string Author { get; set; }
        public int HB_Id { get; set; }

        // Entity Framework => 資料表轉換成物件(類別 Model)
        // insert into  Article ('title', 'date') values('SQLListModel.Title','SQLListModel.date')

    }
}
