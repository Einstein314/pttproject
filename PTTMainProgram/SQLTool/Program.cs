﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


namespace SQLTool
{

    class Program
    {
        public class PTTUser
        {
            public int U_Id { get; set; }
            public string name { get; set; }
            public int score { get; set; }
            public int signindate { get; set; }
            public string lastdate { get; set; }
        }

        static void Main(string[] args)       
        {
            //string datetime = DateTime.Now.ToString("ddd MMM d H:mm:ss yyyy", CultureInfo.CreateSpecificCulture("en-GB"));
            //Console.WriteLine(datetime);
            //Console.ReadKey();
            
            //BaseDB baseDB = new BaseDB("localhost", "Michael", "920411", "PTT_System");

            //BaseDB db = new BaseDB();
            //Dictionary<string, object> dict = new Dictionary<string, object>();
            //dict.Add("email", "a22908352@gmail.con");
            //dict.Add("password", "emailtestlast");
            //PTTUser user = db.NewQuery<PTTUser>("select U_Id,name from PTTUser where email=@email and password=@password", dict).FirstOrDefault();

            //if (user != null)
            //{
            //    MessageBox.Show("登入成功！");
            //    //MainPage mainPage = new MainPage(user.U_Id);
            //    MainPage Main = new MainPage(user.U_Id, user.name);
            //    this.Hide();
            //    //mainPage.Show();
            //    Main.Show();
            //}
            //else
            //{
            //    MessageBox.Show("登入失敗！");
            //}
            //Dictionary<string, object> mydict = new Dictionary<string, object>();
            //mydict.Add("title", "Re: [問卦] 台灣外送市場一年幾百億卻沒管制？");

            //List<SQLListModel> list = baseDB.NewQuery<SQLListModel>(@"SELECT [IB_Id]
            //                                                          ,[Title]
            //                                                          ,[Author]
            //                                                          ,[date]
            //                                                          ,[HB_Id]
            //                                                          FROM [PTT_System].[dbo].[InnerBlock]
            //                                                          where Title = @title", mydict);

            //List<SQLListModel> list = baseDB.NewQuery<SQLListModel>(@"SELECT [IB_Id]
            //                                                         ,[Title]
            //                                                         ,[Author]
            //                                                         ,[date]
            //                                                         ,[HB_Id]
            //                                                         FROM [PTT_System].[dbo].[InnerBlock]
            //                                                         where Title = '{0}' ", "Re: [問卦] 台灣外送市場一年幾百億卻沒管制？");

            //var results = baseDB.NewQuery(@"SELECT TOP 10                                            
            //                                 [Title]
            //                                ,[Author]
            //                                ,[date]
            //                                ,[HB_Id]
            //                                FROM [PTT_System].[dbo].[InnerBlock]");

            //Dapper
            //using (SqlConnection con = new SqlConnection("Persist Security Info=False;User ID=Michael; Password=920411; Initial Catalog=PTT_System; Server=localhost"))
            //{
            //    string sqlstr = @"SELECT TOP 10
            //                       [IB_Id]
            //                      ,[Title]
            //                      ,[Author]
            //                      ,[date]
            //                      ,[HB_Id]
            //                      FROM[PTT_System].[dbo].[InnerBlock]";
            //    var mylist = con.Query(sqlstr).ToList();

            //    foreach (var item in mylist)
            //    {

            //        Console.WriteLine(item.Title);
            //        Console.WriteLine(item.Author);
            //        Console.WriteLine(item.HB_Id);
            //    }

            //}

            //string sqlstr = "INSERT INTO PTTUser(email,password,name) VALUES(@email,@password,@name);";
            //Dictionary<string, object> dict = new Dictionary<string, object>();
            //dict.Add("email", "sqltest@gnail.com");
            //dict.Add("password", 12345);
            //dict.Add("name", "Test");
            //Console.WriteLine(baseDB.NewInsert(sqlstr, dict));

            //foreach (var item in list)
            //{
            //    Console.WriteLine(item.Title);
            //    Console.WriteLine(item.date);
            //    Console.WriteLine(item.Author);
            //    Console.WriteLine(item.HB_Id);
            //}


            // array List
            //Dictionary<string, object> dict = new Dictionary<string, object>();

            //Test("hello {0} {1}", 1, "a");

            //Entity Framework LinQ lambda
            //DB dB = new DB();
            //var user = dB.PTTUser.Select(x=>x.name).ToList();

            //匿名類別 
            //var list = dB.PTTUser.Select(x => new
            //{
            //    x.name,
            //    x.U_Id
            //}).ToList();


            //排序 from where select orderby
            //List<PTTUserModel> list = dB.PTTUser.Where(x => /*x.name.Contains("黃") &&*/ x.email.Contains("gmail")).Select(x => new PTTUserModel
            //{
            //    name = x.name,
            //    U_Id = x.U_Id
            //}).ToList();

            //PTTUserModel first = dB.PTTUser.Select(x => new PTTUserModel
            //{
            //    name = x.name,
            //    U_Id = x.U_Id
            //}).FirstOrDefault();
            //Console.WriteLine(first.name+" "+first.U_Id);

            //var name = list.Select(x=> x.name);
            //foreach(var item in name)
            //{
            //    Console.WriteLine(item);
            //}            



            //新增
            //PTTUser user = new PTTUser();
            //user.name = "linqtest2";
            //user.email = "lingtest@gmail.com2";
            //user.password = "test1";

            //dB.PTTUser.Add(user);

            //dB.SaveChanges();
            //Console.WriteLine(user.U_Id);

            //修改
            //用PK修改
            //PTTUser user = dB.PTTUser.Find(25);            
            //user.name = "linqtest11";
            //dB.SaveChanges();

            //非PK修改
            //PTTUser user = dB.PTTUser.Where(x=>x.name.Equals("linqtest11")).FirstOrDefault();
            //user.name = "linqtest22";
            //dB.SaveChanges();



            //var list = dB.PTTUser.Select(x => new
            //{
            //    x.name,
            //    x.U_Id
            //}).ToList();

            //foreach (var item in list)
            //{
            //    Console.WriteLine(item.name + " " + item.U_Id);
            //}

            //PTTUser user = new PTTUser();
            //user.email = "newtest@gmail.com";
            //user.password = "newtest";
            //user.name = "newtest";

            //baseDB.Insert<PTTUser>(user);

            //PTTUser user1 = baseDB.NewQuery<PTTUser>("select * from PTTUser where U_Id = 27").FirstOrDefault();
            //user1.name = "Leo";
            //baseDB.Update<PTTUser>(user1);

            //PTTUser user1 = baseDB.NewQuery<PTTUser>("select * from PTTUser where U_Id = 29").FirstOrDefault();
            //baseDB.Delete<PTTUser>(user1);

            //Console.ReadKey();
        }
        //public static void Test(String str, params object[] data)  //param ~[]參數型陣列 將,後的資料存成陣列
        //{
        //    for(int i=0; i<data.Length; i++)
        //    {
        //        if(str.Contains("{"+i+"}"))
        //        {
        //            str = str.Replace("{" + i + "}", data[i].ToString());               
        //        }                
        //    }
        //    Console.WriteLine(str); 
        //}
    }
}
